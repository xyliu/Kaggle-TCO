# Copyright 2018 Tony Y. https://www.kaggle.com/tonyyy All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# =========================================================================
import os
import sys
import time
import numpy as np
import pandas as pd
from sklearn.kernel_ridge import KernelRidge
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import make_scorer
from sklearn.preprocessing import MinMaxScaler
from sklearn.externals.joblib import parallel_backend

INPUT_DIR = sys.argv[1]
ngram_size = int(sys.argv[2])
nfold = 5
nthread = 4

df_train = pd.read_csv(INPUT_DIR+"train.csv")
df_train["dataset"] = "train"
df_test = pd.read_csv(INPUT_DIR+"test.csv")
df_test["dataset"] = "test"
df_crystals = pd.concat([df_train, df_test], ignore_index=True)

ngrams = np.load(os.path.join(INPUT_DIR, 'ngrams_cn_quad.npz'))

print('ngram_size: {}'.format(ngram_size))
if ngram_size == 4:
    features = np.hstack([ngrams["unigrams"], ngrams["bigrams"], ngrams["trigrams"], ngrams["quadgrams"]])
elif ngram_size == 3:
    features = np.hstack([ngrams["unigrams"], ngrams["bigrams"], ngrams["trigrams"]])
elif ngram_size == 2:
    features = np.hstack([ngrams["unigrams"], ngrams["bigrams"]])
elif ngram_size == 1:
    features = ngrams["unigrams"]
else:
    raise NameError('ngram_size should be within [1,4].')
print('Ngram feature matrix shape: {}'.format(features.shape))

# Select column
count_per_column = np.sum(features, axis=0)
nonzero_columns = np.where(count_per_column > 0)[0]
print('Total column count: {}'.format(len(count_per_column)))
print('Nonzero-column count: {}'.format(len(nonzero_columns)))

features = features[:,nonzero_columns]
print('Nonzero ngram feature maxrix shape: {}'.format(features.shape))

# Volume
print("Calculate volume..")
lat_a = df_crystals.lattice_vector_1_ang.values
lat_b = df_crystals.lattice_vector_2_ang.values
lat_c = df_crystals.lattice_vector_3_ang.values
lat_cosa = np.cos(np.radians(df_crystals.lattice_angle_alpha_degree.values))
lat_cosb = np.cos(np.radians(df_crystals.lattice_angle_beta_degree.values))
lat_cosg = np.cos(np.radians(df_crystals.lattice_angle_gamma_degree.values))
volume = lat_a * lat_b * lat_c * np.sqrt(1. - lat_cosa**2 - lat_cosb**2 - lat_cosg**2 + 2. * lat_cosa * lat_cosb * lat_cosg)

# Scale
print("Scale..")
features = np.array(features, dtype=np.float)
features = features / volume[:, np.newaxis]
scaler = MinMaxScaler()
features = scaler.fit_transform(features)
print("next:np.log1p")
y1 = np.log1p(df_crystals.formation_energy_ev_natom.values[:len(df_train)])
y2 = np.log1p(df_crystals.bandgap_energy_ev.values[:len(df_train)])
X_train = features[:len(df_train)]
X_test = features[len(df_train):]

def root_mean_squared_error(y_true, y_pred):
    return np.sqrt(np.mean((y_true-y_pred)**2))

neg_root_mean_squared_error = make_scorer(root_mean_squared_error, greater_is_better=False)

def train_and_pred(X_train, y_train, X_test):
    t0 = time.time()
    print("Next: GridSearchCV")
    clf = GridSearchCV(KernelRidge(kernel='rbf'),
                       cv=nfold, n_jobs=nthread, verbose=1,
                       scoring=neg_root_mean_squared_error,
                       param_grid={"alpha": np.logspace(-15, 5, 21, base=2),
                                   "gamma": np.logspace(-15, 3, 19, base=2)})
    #with_parallel_backend('threading'):
    res = clf.fit(X_train, y_train)

    print("clf best_score:", -res.best_score_)
    print("clf best_params:", res.best_params_)
    print("clf fit:", time.time() - t0)
    out_y_train = clf.predict(X_train)
    #for i,j in zip(out_y_train, y_train):
    #    print(i, j)
    t0 = time.time()
    y_test = clf.predict(X_test)
    print("clf predict:", time.time() - t0)

    return clf, y_test, out_y_train

print("next: train_and_predict")
clf1, y1_test, y1_train = train_and_pred(X_train, y1, X_test)
clf2, y2_test, y2_train = train_and_pred(X_train, y2, X_test)

# Find negative values
print('negative values of y1_test:', np.where(y1_test < 0))
print('negative values of y2_test:', np.where(y2_test < 0))

y1_test = np.clip(np.expm1(y1_test), 1e-8, None)
y2_test = np.clip(np.expm1(y2_test), 1e-8, None)

y1_train = np.clip(np.expm1(y1_train), 1e-8, None)
y2_train = np.clip(np.expm1(y2_train), 1e-8, None)

df_sub = pd.read_csv(INPUT_DIR + "sample_submission.csv")
print("shape of y1_test, y2_test:", np.shape(y1_test), np.shape(y2_test))
print("shape of df_sub:", np.shape(df_sub["formation_energy_ev_natom"]))
df_sub["formation_energy_ev_natom"] = y1_test
df_sub["bandgap_energy_ev"] = y2_test
df_sub.to_csv(os.path.join(INPUT_DIR, "submission_krr_ngrams_cn_quad_vol_fine2_n{}.csv".format(ngram_size)), index=False)

df_tmp = pd.read_csv(INPUT_DIR + "train.csv")
df_train = df_tmp[["id","formation_energy_ev_natom", "bandgap_energy_ev"]]
df_train["formation_energy_ev_natom"] = y1_train
df_train["bandgap_energy_ev"] = y2_train
df_train.to_csv(os.path.join(INPUT_DIR, "train_krr_ngrams_cn_quad_vol_fine2_n{}.csv".format(ngram_size)), index=False)

df_tmp = pd.read_csv(INPUT_DIR + "train.csv")
df_train = df_tmp[["id","formation_energy_ev_natom", "bandgap_energy_ev"]]
df_train["formation_energy_ev_natom"] = y1
df_train["bandgap_energy_ev"] = y2
df_train.to_csv(os.path.join(INPUT_DIR, "initial_train_krr_ngrams_cn_quad_vol_fine2_n{}.csv".format(ngram_size)), index=False)
