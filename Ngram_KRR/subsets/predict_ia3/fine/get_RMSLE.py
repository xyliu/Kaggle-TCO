import numpy as np
from numpy import mean, absolute

def MAE(data1, data2):
    if len(data1) == len(data2):
        return mean(absolute(data1 - data2))


def RMSLE(data1,data2):
    if len(data1) == len(data2):
        return np.sqrt(np.mean(np.power(np.log1p(data1)-np.log1p(data2),2)))

test_data = np.loadtxt('/u/xyliu/Kaggle_dataset/subsets/predict_ia3/test.csv', delimiter=",", skiprows=1)

Eform_pred = np.loadtxt('./submission_krr_ngrams_cn_quad_vol_fine2_n4.csv', \
                       delimiter=',', skiprows=1)[:,1]
Egap_pred = np.loadtxt('./submission_krr_ngrams_cn_quad_vol_fine2_n4.csv', \
                       delimiter=',', skiprows=1)[:,2]

Eform_true = test_data[:,-2]
Egap_true = test_data[:,-1]

print(len(Egap_true), len(Egap_pred))

print("Size of test set:", len(Eform_pred))
print("RMSLE of formation energy:", RMSLE(Eform_true,Eform_pred))
print("RMSLE of bandgap", RMSLE(Egap_true,Egap_pred))
print("MAD of formation energy:", MAE(Eform_true,Eform_pred))
print("MAD of bandgap", MAE(Egap_true,Egap_pred))

