# Copyright 2018 Tony Y. https://www.kaggle.com/tonyyy All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# =========================================================================
import os
import numpy as np
import pandas as pd
import crystal_graph as cg
import time
import sys

INPUT_DIR = sys.argv[1]

df_train = pd.read_csv(INPUT_DIR+"train.csv")
df_train["dataset"] = "train"
df_test = pd.read_csv(INPUT_DIR+"test.csv")
df_test["dataset"] = "test"
df_crystals = pd.concat([df_train, df_test], ignore_index=True)

m_cn_min = 4
m_cn_max = 9
o_cn_min = 2
o_cn_max = 6

def check_crdn(crdn, symbols, filename):
    metals = [i for i, s in enumerate(symbols) if s != 'O']
    oxygens = [i for i, s in enumerate(symbols) if s == 'O']
    metal_cn = [len(crdn[i]) for i in metals]
    oxygen_cn = [len(crdn[i]) for i in oxygens]
    if np.max(metal_cn) > m_cn_max or np.min(metal_cn) < m_cn_min:
        print(filename)
        print('metal_cn: {}'.format(metal_cn))
    if np.max(oxygen_cn) > o_cn_max or np.min(oxygen_cn) < o_cn_min:
        print(filename)
        print('oxygen_cn: {}'.format(oxygen_cn))

def get_cg(fn, factor):
    crystal_xyz, crystal_sym, crystal_lat = cg.get_xyz_data(fn)
    A = np.transpose(crystal_lat)
    B = np.linalg.inv(A)
    crystal_red = np.matmul(crystal_xyz, np.transpose(B))
    R_max = cg.get_Rmax(crystal_sym) * factor
    crystal_dist, crystal_Rij, crystal_crdn = cg.get_shortest_distances(crystal_red, A, R_max)

    check_crdn(crystal_crdn, crystal_sym, fn)

    return crystal_crdn, crystal_sym

m_elements = ['Al', 'Ga', 'In']
m_tokens = []
for i in range(m_cn_min, m_cn_max+1):
    m_tokens += [ s+str(i) for s in m_elements]
o_tokens = ['O'+str(i) for i in range(o_cn_min, o_cn_max+1)]
tokens = m_tokens + o_tokens
print(tokens)
token_dict = dict([(tokens[i], i) for i in range(len(tokens))])

def get_symbols(G):
    return G[1]

def coord_number(G, node):
    return len(G[0][node])

def neighbors(G, node):
    return [c[0] for c in G[0][node]]

def neighbors_with_R(G, node):
    return [(c[0], c[2]) for c in G[0][node]]

def get_unigrams(G):
    symbols = get_symbols(G)
    symbols = [token_dict[s + str(coord_number(G, i))] for i, s in enumerate(symbols)]
    uniques, counts = np.unique(symbols, return_counts=True)
    counts = dict(zip(list(uniques), list(counts)))
    v = np.zeros(len(tokens), dtype=np.int)
    for key, value in counts.items():
        v[key] = value

    return v

pairs = []
for m in m_tokens:
    pairs += [ m + '_' + o for o in o_tokens]
print(pairs)
pair_dict = dict([(pairs[i], i) for i in range(len(pairs))])

def get_bigrams(G):
    symbols = get_symbols(G)
    metals = [i for i, s in enumerate(symbols) if s != 'O']
    symbols = [s + str(coord_number(G, i)) for i, s in enumerate(symbols)]
    pair_list = []
    for j in metals:
        sym_j = symbols[j]
        for i in neighbors(G, j):
            sym_i = symbols[i]
            pair = sym_j + '_' + sym_i
            pair_list.append(pair_dict[pair])

    uniques, counts = np.unique(pair_list, return_counts=True)
    counts = dict(zip(list(uniques), list(counts)))
    v = np.zeros(len(pairs), dtype=np.int)
    for key, value in counts.items():
        v[key] = value

    return v

mom_triples = []
for m1 in range(len(m_tokens)):
    for m2 in range(m1+1):
        mom_triples += [m_tokens[m1] + '_' + o + '_' + m_tokens[m2] for o in o_tokens]
omo_triples = []
for o1 in range(len(o_tokens)):
    for o2 in range(o1+1):
        omo_triples += [o_tokens[o1] + '_' + m + '_' + o_tokens[o2] for m in m_tokens]
triples = mom_triples + omo_triples
print(triples[:10])
triples_dict = dict([(triples[i], i) for i in range(len(triples))])

def get_trigrams(G):
    symbols = get_symbols(G)
    oxygens = [i for i, s in enumerate(symbols) if s == 'O']
    metals = [i for i, s in enumerate(symbols) if s != 'O']
    symbols = [s + str(coord_number(G, i)) for i, s in enumerate(symbols)]
    triple_list = []
    for o in oxygens:
        sym_o = symbols[o]
        nbr = neighbors(G, o)
        for j in range(len(nbr)):
            sym_j = symbols[nbr[j]]
            for i in range(j):
                sym_i = symbols[nbr[i]]
                if token_dict[sym_i] >= token_dict[sym_j]:
                    triple = sym_i + '_' + sym_o + '_' + sym_j
                else:
                    triple = sym_j + '_' + sym_o + '_' + sym_i
                triple_list.append(triples_dict[triple])
    for m in metals:
        sym_m = symbols[m]
        nbr = neighbors(G, m)
        for j in range(len(nbr)):
            sym_j = symbols[nbr[j]]
            for i in range(j):
                sym_i = symbols[nbr[i]]
                if token_dict[sym_i] >= token_dict[sym_j]:
                    triple = sym_i + '_' + sym_m + '_' + sym_j
                else:
                    triple = sym_j + '_' + sym_m + '_' + sym_i
                triple_list.append(triples_dict[triple])

    uniques, counts = np.unique(triple_list, return_counts=True)
    counts = dict(zip(list(uniques), list(counts)))
    v = np.zeros(len(triples), dtype=np.int)
    for key, value in counts.items():
        v[key] = value

    return v

quads = []
for m1 in m_tokens:
    for o1 in o_tokens:
        for m2 in m_tokens:
            quads += [m1 + '_' + o1 + '_' + m2 + '_' + o for o in o_tokens]
print(quads[:10])
quad_dict = dict([(quads[i], i) for i in range(len(quads))])

def get_quadgrams(G):
    symbols = get_symbols(G)
    oxygens = [i for i, s in enumerate(symbols) if s == 'O']
    metals = [i for i, s in enumerate(symbols) if s != 'O']
    symbols = [s + str(coord_number(G, i)) for i, s in enumerate(symbols)]
    quad_list = []
    for o in oxygens:
        sym_o = symbols[o]
        nbr = neighbors_with_R(G, o)
        for j in range(len(nbr)):
            nbr_j = neighbors_with_R(G, nbr[j][0])
            sym_j = symbols[nbr[j][0]]
            for i in range(j):
                nbr_i = neighbors_with_R(G, nbr[i][0])
                sym_i = symbols[nbr[i][0]]
                triple1 = sym_i + '_' + sym_o + '_' + sym_j
                triple2 = sym_j + '_' + sym_o + '_' + sym_i
                for kj in nbr_j:
                    if kj[0] == o and np.sum(np.abs(nbr[j][1]+kj[1])) < 1e-8: continue
                    quad1 = triple1 + '_' + symbols[kj[0]]
                    quad_list.append(quad_dict[quad1])
                for ki in nbr_i:
                    if ki[0] == o and np.sum(np.abs(nbr[i][1]+ki[1])) < 1e-8: continue
                    quad2 = triple2 + '_' + symbols[ki[0]]
                    quad_list.append(quad_dict[quad2])

    uniques, counts = np.unique(quad_list, return_counts=True)
    counts = dict(zip(list(uniques), list(counts)))
    v = np.zeros(len(quads), dtype=np.int)
    for key, value in counts.items():
        v[key] = value

    return v

unigrams = []
bigrams = []
trigrams = []
quadgrams = []

count = 0
t0 = time.time()
for crystal in df_crystals.itertuples():
    fn = INPUT_DIR + "{}/{}/geometry.xyz".format(crystal.dataset, crystal.id)
    factor = cg.get_factor(crystal.spacegroup, crystal.lattice_angle_gamma_degree)
    G = get_cg(fn, factor)
    unigrams.append(get_unigrams(G))
    bigrams.append(get_bigrams(G))
    trigrams.append(get_trigrams(G))
    quadgrams.append(get_quadgrams(G))

    count += 1
    if count % 500 == 0:
        print(count, time.time()-t0)

#print(np.array(unigrams))
#print(np.array(bigrams))
#print(np.array(trigrams))
#print(np.array(quadgrams))

print(len(tokens))
print(len(pairs))
print(len(triples))
print(len(quads))

np.savez_compressed(os.path.join(INPUT_DIR,'ngrams_cn_quad'),
                    tokens=tokens, pairs=pairs, triples=triples, quads=quads,
                    unigrams=unigrams, bigrams=bigrams, trigrams=trigrams, quadgrams=quadgrams)
