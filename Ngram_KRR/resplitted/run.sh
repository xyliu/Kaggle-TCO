#!/bin/bash
# =========================================================================
# Copyright 2018 Tony Y. https://www.kaggle.com/tonyyy All Rights Reserved.
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# =========================================================================
INPUT_DIR=$1

#echo "Generating ngrams..."
#python src/gen_ngrams_cn_wonx_quad.py $INPUT_DIR

echo "Fitting the KRR model with trigrams..."
python src/krr_ngrams_cn_quad_vol_fine2.py $INPUT_DIR 1

echo "Fitting the KRR model with trigrams..."
python src/krr_ngrams_cn_quad_vol_fine2.py $INPUT_DIR 2

echo "Fitting the KRR model with trigrams..."
python src/krr_ngrams_cn_quad_vol_fine2.py $INPUT_DIR 3

echo "Fitting the KRR model with quadgrams..."
python src/krr_ngrams_cn_quad_vol_fine2.py $INPUT_DIR 4

echo "Fitting the mixed ensemble model of two KRR model..."
python src/krr_ngrams_cn_quad_vol_mixed2.py $INPUT_DIR

