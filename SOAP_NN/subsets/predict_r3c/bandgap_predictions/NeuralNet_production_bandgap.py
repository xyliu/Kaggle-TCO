#!/usr/bin/env python
import sys
import os
import time
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
import torch
from torch.autograd import Variable
import torch.nn as nn
import torch.nn.functional as F
torch.manual_seed(1)
print(sys.version)


##################
'''
if(len(sys.argv)!=2):
    print("\nUsage: python Neural.....py split_dir\n\n")
    sys.exit()
else:
    split_dir = sys.argv[-1]
    print("Training set:", split_dir)

'''

#############





############################################################################### 
class myNNRegressor(torch.nn.Module):
    def __init__(self, feature_size):
         super(myNNRegressor, self).__init__()
         self.feature_size = feature_size
         self.hidden_size1 = 512
         self.hidden_size2 = 256
         self.linear1 = nn.Linear(self.feature_size, self.hidden_size1, bias=False)
         self.batchnorm1 = nn.BatchNorm1d(self.hidden_size1, affine=True)
         self.linear2 = nn.Linear(self.hidden_size1, self.hidden_size2, bias=False)
         self.batchnorm2 = nn.BatchNorm1d(self.hidden_size2, affine=True)
         self.linear3 = nn.Linear(self.hidden_size2, self.hidden_size2, bias=False)
         self.batchnorm3 = nn.BatchNorm1d(self.hidden_size2, affine=True)
         self.h2s = nn.Linear(self.hidden_size2, 1, bias=False)
         self.activation = nn.ReLU()
         self.dropout = nn.Dropout(p=0.20)

    def forward(self, X):
        h1 = self.activation(self.dropout(self.batchnorm1(self.linear1(X))))
        h2 = self.activation(self.dropout(self.batchnorm2(self.linear2(h1))))
        h3 = self.activation(self.dropout(self.batchnorm3(self.linear3(h2))))
        predictions = self.h2s(h3)
        return predictions
############################################################################### 

def rmsle(real, predicted):
    sum=0.0
    _ = predicted.cpu().data.numpy()
    for idx, x in enumerate(_):
        if x<0:
            continue
        p = torch.log(predicted[idx]+1)
        r = torch.log(real[idx]+1)
        sum += (p - r)**2
    return torch.sqrt(sum/float(len(predicted)))

def get_predictions(nEpochs, X_train, y_train, X_val=None, y_val=None, evaluation=False):
    try:
        os.remove('./myBestModel.pt')
    except:
        pass

    feature_size = X_train.shape[1]
    #feature_size = X_train.shape[0]
    print("size of feature (in training set):", feature_size)
    learning_rate = 1e-3

    X_train = Variable(torch.from_numpy(X_train).float(),
            requires_grad = False).cpu()
    y_train = Variable(torch.from_numpy(y_train).float(),
            requires_grad = False).cpu()
    if evaluation:
        X_val = Variable(torch.from_numpy(X_val).float(),
                requires_grad = False, volatile = True).cpu()
        y_val = Variable(torch.from_numpy(y_val).float(),
                requires_grad = False, volatile = True).cpu()

    NNRegressor = myNNRegressor(feature_size).cpu()
    optimiser = torch.optim.Adam(NNRegressor.parameters(), lr=learning_rate)
    loss_fn = nn.MSELoss()

    y_train = y_train.reshape(-1, 1)

    for epoch in range(nEpochs):
        print('____\nEpoch: {}'.format(epoch))
        #print("Shape of X_train, y_train(y_enthalpy):", np.shape(X_train), np.shape(y_train))

        # Train
        NNRegressor.train(True)
        t0 = time.time()
        predictions = NNRegressor(X_train)

        #print("\nShape of predictions:", np.shape(predictions))
        #print("Shape of y_train:", np.shape(y_train))

        if epoch <= 100:
            loss = loss_fn(predictions, y_train)
        else:
            loss = rmsle(y_train, predictions)
        print("loss:", loss)

        NNRegressor.zero_grad()
        loss.backward()
        torch.nn.utils.clip_grad_norm(NNRegressor.parameters(), 1.0)
        optimiser.step()
        t1 = time.time()

        # Evaluate
        NNRegressor.train(False)
        predictions_train = NNRegressor(X_train)

        train_loss = rmsle(y_train, predictions_train).data[0]
        if evaluation:
            predictions = NNRegressor(X_val)
            val_loss = rmsle(y_val, predictions).data[0]
        NNRegressor.train(True)
        log = open('training.log', 'a')
        if evaluation:
            log.write('epoch {0}: {1:.2f} sec - train loss: {2} - val. loss: {3}\n'.format(
                epoch, t1-t0, train_loss, val_loss))
        else:
            log.write('epoch {0}: {1:.2f} sec - train loss: {2}\n'.format(
                epoch, t1-t0, train_loss))
        log.close()

        if evaluation:
            if 'best_val_loss' in locals():
                if val_loss < best_val_loss:
                    best_val_loss = val_loss
                    with open('myBestModel.pt', 'wb') as f:
                        torch.save(NNRegressor, f)
            else:
                best_val_loss = val_loss
                with open('myBestModel.pt', 'wb') as f:
                    torch.save(NNRegressor, f)

        if epoch == 149:
            learning_rate=1e-5
            if evaluation:
                del NNRegressor
                with open('myBestModel.pt', 'rb') as f:
                   NNRegressor = torch.load(f)
            optimiser = torch.optim.Adam(NNRegressor.parameters(),
                lr=learning_rate)

    if evaluation:
        with open('myBestModel.pt', 'rb') as f:
            NNRegressor = torch.load(f)
        NNRegressor.train(False)
        predictions = NNRegressor(X_val)
        val_loss = rmsle(y_val, predictions).data[0]
        log = open('training.log', 'a')
        log.write('-----\nbest val. loss: {}\n_____'.format(val_loss))
        log.close()
    else:
        NNRegressor.train(False)
        predictions = NNRegressor(X_train)
    return predictions, NNRegressor

###############################################################################
#twin_idx_original is the list in the original 2400 dataset
twin_idx_original = [125, 1378, 1885, 307, 2332] # these compounds have a twin in the 

                                        # data set and the bandgap and the 
                                        # formation energies of their twins
                                        # agree signficiantly better with the
                                        # predicted values
twin_idx = []

data_path = '/u/xyliu/Kaggle_dataset/subsets/predict_r3c'
descriptor_path = '/u/xyliu/Kaggle_dataset/subsets/predict_r3c'
#mean_atomic_descriptors = 'atomic_descriptors_10_4_4'
mean_atomic_descriptors = ''


scaler = StandardScaler(with_mean=True, with_std=True)

### load data
test_csv = np.loadtxt(data_path+'/test.csv', skiprows=1, delimiter=',')
test_mean_atomic_descriptors = np.load(descriptor_path + '/' \
        + mean_atomic_descriptors + '/test_mean_atomic_descriptors.npy')

print("Test set loaded from ", data_path+'/test.csv')
print("Test set descriptors loaded from ", descriptor_path + '/'+ mean_atomic_descriptors + '/test_mean_atomic_descriptors.npy')
print("shape of test atomic_descriptors:", np.shape(test_mean_atomic_descriptors), np.min(test_mean_atomic_descriptors), np.max(test_mean_atomic_descriptors))

train_csv = np.loadtxt(data_path+'/train.csv', skiprows=1, delimiter=',')
print("Training set loaded from ", data_path+'/train.csv')

twin_enthalpy = train_csv[twin_idx, -2]
twin_bandgap = train_csv[twin_idx, -1]
train_csv = np.delete(train_csv, twin_idx, axis=0)
y_enthalpy = 10*train_csv[:, -2]
y_bandgap = train_csv[:, -1]
print("shape of y_bandgap:", np.shape(y_bandgap))


train_mean_atomic_descriptors = np.load(descriptor_path + '/' \
        + mean_atomic_descriptors + '/train_mean_atomic_descriptors.npy')
print("Training set descriptors loaded from ", descriptor_path + '/'+ mean_atomic_descriptors + '/train_mean_atomic_descriptors.npy')
print("shape of train atomic_descriptors:", np.shape(train_mean_atomic_descriptors))
print("train_mean_atomic_descriptors:", np.min(train_mean_atomic_descriptors), np.max(train_mean_atomic_descriptors), np.argwhere(np.isnan(train_mean_atomic_descriptors)))

combined_mean_atomic_descriptors = scaler.fit_transform(np.append(
        test_mean_atomic_descriptors,
        train_mean_atomic_descriptors,
        axis=0))

X_test = np.round(combined_mean_atomic_descriptors[:test_csv.shape[0]],
        decimals=5)
X_test = Variable(torch.from_numpy(X_test).float(),
                requires_grad = False, volatile = True).cpu()
#X_test = X_test.reshape(-1, 1)
print("shape of X_test:", np.shape(X_test))
X_train = np.round(combined_mean_atomic_descriptors[test_csv.shape[0]:],
        decimals=5)
X_twins = Variable(torch.from_numpy(X_train[twin_idx]).float(),
                requires_grad = False, volatile = True).cpu()
X_train = np.delete(X_train, twin_idx, axis=0)
#X_train = X_train.reshape(-1, 1)
print("shape of X_train:", np.shape(X_train))

###############################################################################

ensemble_size = 200
try:
    os.remove('./training.log')
except:
    pass
for regressor in range(ensemble_size):
    log = open('training.log', 'a')
    log.write('\n________________\nRegressor No. {}\n'.format(regressor))
    log.close()


    predictions, NNRegressor = get_predictions(nEpochs=250,
            X_train=X_train, y_train=y_bandgap)
    NNRegressor.train(False)
    
    '''
    twin_predictions = NNRegressor(X_twins)
    _ = np.append(np.array(twin_idx).reshape(-1, 1),
            twin_predictions.data.numpy().reshape(-1, 1),
            axis=1)
    _ = np.append(_, np.array(twin_bandgap).reshape(-1, 1),
            axis=1)
    _ = np.append(_, np.array(twin_enthalpy).reshape(-1, 1),
            axis=1)
    
    log = open('training.log', 'a')
    log.write('\nTWINs:\nid, prediction, bandgap, enthalpy\n')
    log.write('{}'.format(np.round(_,decimals=2)))
    log.close()
    '''
    predictions = NNRegressor(X_test).cpu().data.numpy()
    train_predictions = NNRegressor(
                            Variable(torch.from_numpy(X_train).float(),
                                requires_grad=False, volatile=True)
                                ).cpu().data.numpy()
    if 'all_predictions' in locals():
        all_predictions = np.append(all_predictions, predictions, axis=1)
        all_train_predictions = np.append(all_train_predictions,
                                            train_predictions, axis=1)
    else:
        all_predictions = predictions
        all_train_predictions = train_predictions
final_predictions = np.mean(all_predictions, axis=1)
final_train_predictions = np.mean(all_train_predictions, axis=1)
np.savetxt('submission.csv', np.append(test_csv[:, 0].reshape(-1,1),
        final_predictions.reshape(-1,1), axis=1), header='id, prediction')
np.savetxt('train_prediction.csv', np.append(train_csv[:, 0].reshape(-1,1),
        final_train_predictions.reshape(-1,1), axis=1), header='id, prediction')

