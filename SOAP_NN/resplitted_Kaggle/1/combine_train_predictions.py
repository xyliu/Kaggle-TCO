import numpy as np
import sys, os
import pandas as pd

def split(row, idx):
    tmp = row.tolist()[0]
    if idx == 0:
        return int(float(tmp.split()[idx]))
    elif idx == 1:
        return float(tmp.split()[idx])
   
df_Eg = pd.read_csv(os.path.join('./', "bandgap_predictions/train_prediction.csv"))
df_Ef = pd.read_csv(os.path.join('./', "enthalpy_predictions/train_prediction.csv"))

df_Eg["Eg"] = df_Eg.apply(lambda row: split(row, 1), axis=1)
df_Eg["id"] = df_Eg.apply(lambda row: split(row, 0), axis=1)    
df_Ef["tmp_Ef"] = df_Ef.apply(lambda row: split(row, 1), axis=1)
df_Ef["id"] = df_Ef.apply(lambda row: split(row, 0), axis=1)    
df_Ef["Ef"] = df_Ef["tmp_Ef"]/10.0

tmp_df1 = df_Eg[["Eg","id"]]
tmp_df2 = df_Ef[["Ef","id"]]

tmp_df2.reset_index()
tmp_df1.reset_index()
final_df = pd.merge(tmp_df1, tmp_df2, on="id")

final_df.to_csv("SOAP_NN_train_predictions.csv", index=False)


# if label == "train":
#       df = pd.read_csv(os.path.join('/Users/Chris/Dropbox/calcs/FHI/main_plots_kaggle_competition/data_files/041118_data_files/', "041118_ECN_summary_train.csv"))
#       df = df.rename(columns={'subdir' : 'id'})
#       tmp_df = pd.read_csv(os.path.join('/Users/Chris/Dropbox/nomad2018_kaggle_codes/data_files/for_sisso', 'train.csv'))

# elif label == "test":
#       df = pd.read_csv(os.path.join('/Users/Chris/Dropbox/calcs/FHI/main_plots_kaggle_competition/data_files/041118_data_files/', "041118_ECN_summary_test.csv"))
#       df = df.rename(columns={'subdir' : 'id'})
#       tmp_df = pd.read_csv(os.path.join('/Users/Chris/Dropbox/nomad2018_kaggle_codes/data_files/for_sisso', 'test.csv'))


