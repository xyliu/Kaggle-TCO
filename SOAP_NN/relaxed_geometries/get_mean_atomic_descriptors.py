#!/usr/bin/env python
import sys
import numpy as np
import pandas as pd
import ase
from quippy.atoms import Atoms as QuippyAtoms
from ase.atoms import Atoms as ASEAtoms
from ase.io import read, write
import quippy as qp
from quippy import descriptors
print(sys.version)

def get_atomic_soap_descriptors(geometry_xyz):
    # get cell vectors from *.xyz file
    cell_ = np.loadtxt(geometry_xyz, skiprows=4, usecols=(1,2,3))[:3]
    # get atom positions and types from *.xyz file
    atoms_ = pd.read_table(geometry_xyz, skiprows=7,
        delim_whitespace=True, header=None, names=['x', 'y', 'z', 'type'],
        usecols=[1,2,3,4])
    # construct ase Atoms object
    compound = ASEAtoms(atoms_.type.values,
                     positions = atoms_.values[:, :3],
                     cell = cell_,
                     pbc=[1, 1, 1])
    compound = QuippyAtoms(compound)

    ###########################################################################
    # The specification of which atoms are used as SOAP centers is separate to the
    # specification of which atoms are taken into account in the environment.
    # The n_Z=2 Z={1 6} options specify that both H and C are to be taken as SOAP
    # centers. The n_species=2 species_Z={1 6} options specify the atom types to
    # be taken into account in the environment.
    desc = descriptors.Descriptor(
    "soap cutoff=10 l_max=4 n_max=4 atom_sigma=0.5 n_Z=4 Z={8 13 31 49} n_species=4 species_Z={8 13 31 49}") # for formation energies
    #"soap cutoff=20 l_max=6 n_max=10 atom_sigma=0.5 n_Z=1 Z={8} n_species=4 species_Z={8 13 31 49}") # for bandgaps
    compound.set_cutoff(desc.cutoff())
    compound.calc_connect()
    return desc.calc(compound)['descriptor']

#twin_idx = [125, 394, 530, 1378, 1214, 1885, 352, 2074, 307, 2153, 2318, 2336,
#            2332, 2369] #, 1552, 2611] 2611 is id=212 in test

###############################################################################
root = './'
train_data = './train'
test_data = './test'

# train_csv header: 
# id,spacegroup,#total_atoms, %Al, %Ga, %In, a, b, c, alpha, beta, gamma, formation_energy_ev_natom,bandgap_energy_ev
#  0,         1,           2,   3,   4,   5, 6, 7, 8,     9,   10,    11,                        12,      13
train_csv = np.loadtxt(root+'/train.csv', skiprows=1, delimiter=',')
test_csv = np.loadtxt(root+'/test.csv', skiprows=1, delimiter=',')
print('train_csv.shape:\n{}'.format(train_csv.shape))
###############################################################################

###############################################################################
for i_compound, compound_id in enumerate(test_csv[:, 0].astype(int)):
    print(i_compound, 'compound_id: {}.'.format(compound_id))
    compound_xyz = test_data+'/'+str(i_compound+1)+'/geometry.xyz'
    mean_atomic_descriptor =  np.mean(get_atomic_soap_descriptors(compound_xyz),
                                      axis=0)

    if 'mean_atomic_descriptors' in locals():
        mean_atomic_descriptors = np.append(mean_atomic_descriptors,
                                mean_atomic_descriptor.reshape(1, -1), axis=0)
    else:
        mean_atomic_descriptors = mean_atomic_descriptor.reshape(1, -1)
        print('mean_atomic_descriptor.shape: {}'.format(
            mean_atomic_descriptor.shape))

np.save('test_mean_atomic_descriptors.npy', mean_atomic_descriptors)
print('test_mean_atomic_descriptors.shape: {}'.format(mean_atomic_descriptors.shape))
print('test_mean_atomic_descriptors: done.')

del mean_atomic_descriptors
for i_compound, compound_id in enumerate(train_csv[:, 0].astype(int)):
    print(i_compound, 'compound_id: {}.'.format(compound_id))
    compound_xyz = train_data+'/'+str(i_compound+1)+'/geometry.xyz'
    mean_atomic_descriptor =  np.mean(get_atomic_soap_descriptors(compound_xyz),
                                      axis=0)

    if 'mean_atomic_descriptors' in locals():
        mean_atomic_descriptors = np.append(mean_atomic_descriptors,
                                mean_atomic_descriptor.reshape(1, -1), axis=0)
    else:
        mean_atomic_descriptors = mean_atomic_descriptor.reshape(1, -1)
        print('mean_atomic_descriptor.shape: {}'.format(
            mean_atomic_descriptor.shape))

np.save('train_mean_atomic_descriptors.npy', mean_atomic_descriptors)
print('train_mean_atomic_descriptors.shape: {}'.format(mean_atomic_descriptors.shape))
print('train_mean_atomic_descriptors: done.')

