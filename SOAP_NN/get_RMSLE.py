import numpy as np
import sys


if(len(sys.argv)!=4):
    print("\nUsage: python get.....py form(gap) test_true.csv submission.csv\n\n")
    sys.exit()



def RMSLE(data1,data2):
    if len(data1) == len(data2):
        return np.sqrt(np.mean(np.power(np.log1p(data1)-np.log1p(data2),2)))
print("True data loaded from ", sys.argv[2])
true_data = np.loadtxt(sys.argv[2], delimiter=",", skiprows=1)


Eform_true = true_data[:,-2]
Egap_true = true_data[:,-1]

pred_file = sys.argv[3]
print("Predictions loaded from ", pred_file)
E_pred = np.loadtxt(pred_file, delimiter=' ', skiprows=1)[:,1]

pred_prop = sys.argv[1]
if(pred_prop.find("form")!=-1):
    print("RMSLE of formation energy:", RMSLE(Eform_true,E_pred))
elif(pred_prop.find("gap")!=-1):
    print("RMSLE of bandgap:", RMSLE(Egap_true,E_pred))
