import os, sys
import pandas as pd
from shutil import copyfile

INPUT_DIR = sys.argv[1]
rootdir1="/u/xyliu/Kaggle_dataset/resplitted/master_dirs_combined/"
rootdir2="/u/xyliu/Kaggle_dataset/resplitted/"

for fname in ["train", "test"]:
    df = pd.read_csv(INPUT_DIR+fname+".csv")
    dirname = os.path.join(rootdir2, INPUT_DIR, "renamed_"+fname)
    if not os.path.exists(dirname):
        os.makedirs(dirname)

    for fname2 in df.id.tolist():
        if not os.path.exists(os.path.join(dirname, str(fname2))):
            os.makedirs(os.path.join(dirname, str(fname2)))

        copyfile(os.path.join(rootdir1, str(fname2), "geometry.xyz"), os.path.join(dirname, str(fname2), "geometry.xyz"))

#df_train["dataset"] = "train"
#df_test = pd.read_csv(INPUT_DIR+"test.csv")
#df_test["dataset"] = "test"
#df_crystals = pd.concat([df_train, df_test], ignore_index=True)

