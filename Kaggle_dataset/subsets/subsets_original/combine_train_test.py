import glob
import numpy as np
import pandas as pd

train_csv_list= glob.glob("./*train*.csv")
test_csv_list= glob.glob("./*test*.csv")
train_csv_list.sort()
test_csv_list.sort()
print(train_csv_list)
print(test_csv_list)
assert len(train_csv_list) == len(test_csv_list)

for i in range(len(train_csv_list)):
    assert train_csv_list[i][train_csv_list[i].find("_")+1:-4] == test_csv_list[i][test_csv_list[i].find("_")+1:-4]
    name = train_csv_list[i][train_csv_list[i].find("_")+1:-4]
    
    train_csv_obj = pd.read_csv(train_csv_list[i])
    test_csv_obj = pd.read_csv(test_csv_list[i])
    test_csv_obj['id'] += 2400


    with open('./all_' + name + '.csv', 'w') as f:
        train_csv_obj.to_csv(f, index=False)
    with open('./all_' + name + '.csv', 'a') as f:
        test_csv_obj.to_csv(f, header = False, index=False)
 
    print("Done ", name, '\n')

