import numpy as np
import pandas as pd

def select_spacegroup_subset(spacegroup, data_csv, csv_file_name):
    subset_csv = data_csv[data_csv['spacegroup'] == spacegroup]
    subset_csv.to_csv(csv_file_name, sep=',', index=False)
    return subset_csv


#training set

train_csv_obj=pd.read_csv('../../data/train.csv')

ia3_subset_train = select_spacegroup_subset(206, train_csv_obj, 'train_ia3.csv')
ia3_subset_train_index =  [x+1 for x in ia3_subset_train.index.tolist() ]
np.savetxt("./train_ia3_index", ia3_subset_train_index, fmt="%d")


c2m_subset_train = select_spacegroup_subset(12, train_csv_obj, 'train_c2m.csv')
c2m_subset_train_index =  [x+1 for x in  c2m_subset_train.index.tolist() ]
np.savetxt("./train_c2m_index", c2m_subset_train_index, fmt="%d")

pna21_subset_train = select_spacegroup_subset(33, train_csv_obj, 'train_pna21.csv')
pna21_subset_train_index =  [x+1 for x in pna21_subset_train.index.tolist() ]
np.savetxt("./train_pna21_index", pna21_subset_train_index, fmt="%d")

R3c_subset_train = select_spacegroup_subset(167, train_csv_obj, 'train_r3c.csv')
R3c_subset_train_index =  [x+1 for x in R3c_subset_train.index.tolist() ]
np.savetxt("./train_r3c_index", R3c_subset_train_index, fmt="%d")


fd3m_subset_train = select_spacegroup_subset(227, train_csv_obj, 'train_fd3m.csv')
fd3m_subset_train_index =  [x+1 for x in fd3m_subset_train.index.tolist() ]
np.savetxt("./train_fd3m_index", fd3m_subset_train_index, fmt="%d")

p63mmc_subset_train = select_spacegroup_subset(194, train_csv_obj, 'train_p63mmc.csv')
p63mmc_subset_train_index =  [x+1 for x in p63mmc_subset_train.index.tolist() ]
np.savetxt("./train_p63mmc_index", p63mmc_subset_train_index, fmt="%d")



#test set
test_csv_obj=pd.read_csv('../../data/labeled_test.csv')

ia3_subset_test = select_spacegroup_subset(206, test_csv_obj, 'test_ia3.csv')
ia3_subset_test_index =  [x+1 for x in ia3_subset_test.index.tolist()]
np.savetxt("./test_ia3_index", ia3_subset_test_index, fmt="%d")


c2m_subset_test = select_spacegroup_subset(12, test_csv_obj, 'test_c2m.csv')
c2m_subset_test_index =  [x+1 for x in  c2m_subset_test.index.tolist() ]
np.savetxt("./test_c2m_index", c2m_subset_test_index, fmt="%d")

pna21_subset_test = select_spacegroup_subset(33, test_csv_obj, 'test_pna21.csv')
pna21_subset_test_index =  [x+1 for x in  pna21_subset_test.index.tolist()]
np.savetxt("./test_pna21_index", pna21_subset_test_index, fmt="%d")

R3c_subset_test = select_spacegroup_subset(167, test_csv_obj, 'test_r3c.csv')
R3c_subset_test_index =  [x+1 for x in R3c_subset_test.index.tolist()]
np.savetxt("./test_r3c_index", R3c_subset_test_index, fmt="%d")

fd3m_subset_test = select_spacegroup_subset(227, test_csv_obj, 'test_fd3m.csv')
fd3m_subset_test_index =  [x+1 for x in fd3m_subset_test.index.tolist() ]
np.savetxt("./test_fd3m_index", fd3m_subset_test_index, fmt="%d")


p63mmc_subset_test = select_spacegroup_subset(194, test_csv_obj, 'test_p63mmc.csv')
p63mmc_subset_test_index =  [x+1 for x in p63mmc_subset_test.index.tolist() ]
np.savetxt("./test_p63mmc_index", p63mmc_subset_test_index, fmt="%d")

