import pandas as pd
import tqdm
import numpy as np
from collections import defaultdict
from helper_repr import special_xyz_read
from ase.neighborlist import NeighborList


#################Training set################################

print("Reading training data...")
atom_list=[]
for i in range(1,2401):
    atoms=special_xyz_read("train/%d/geometry.xyz"%i)
    atoms.id=i
    atom_list.append(atoms)


print("Generating feature dictionary...")
new_feature_dict=defaultdict(list)

for atoms in tqdm.tqdm_notebook(atom_list):
    metals_mask_dict={el:np.array(atoms.get_chemical_symbols())==el for el in ["Al","Ga","In"]}
    nl = NeighborList([2.5/2.]*len(atoms),self_interaction=False, bothways=True,skin=0.0)
    nl.update(atoms)
    vectors=[]
    distances=[]
    for ind in np.arange(len(atoms)):
        indices, offsets = nl.get_neighbors(ind)
        vecs=np.array([atoms.positions[i] + np.dot(offset, atoms.get_cell()) for i, offset in zip(indices, offsets)])-atoms.positions[ind] 
        vectors.append(vecs)
        dists=np.linalg.norm(vecs,axis=1)
        distances.append(dists)   

    vectors=np.array(vectors)
    distances=np.array(distances)


    for el, metals_mask in metals_mask_dict.items():
        dist=list(map(np.mean,distances[metals_mask]))
        new_feature_dict[el+"_nn_dist_mean"].append(np.mean(dist)  if len(dist)>0 else 10)
        new_feature_dict[el+"_nn_dist_std"].append(np.std(dist)  if len(dist)>0 else 10)
        new_feature_dict[el+"_nn_dist_count"].append(np.mean(list(map(len,distances[metals_mask]))))
        centrosym_vecs_norms=[np.linalg.norm(np.array(r).sum(axis=0)) for r in vectors[metals_mask]]
        new_feature_dict[el+"_nn_centrosym_mean"].append(np.mean(centrosym_vecs_norms) if len(centrosym_vecs_norms)>0 else 10)
        new_feature_dict[el+"_nn_centrosym_std"].append(np.std(centrosym_vecs_norms)  if len(centrosym_vecs_norms)>0 else 10)

    new_feature_dict["id"].append(atoms.id)


geom_features=pd.DataFrame(new_feature_dict)
geom_features=geom_features.fillna(0)

geom_features.to_csv("geom_features_train.csv")
print("Geometry features of training set saved to geom_features_train.csv.")




##################Test set##################################

print("Reading test set...")

atom_list_test=[]
for i in range(1,601):
    atoms=special_xyz_read("test/%d/geometry.xyz"%i)
    atoms.id=i
    atom_list_test.append(atoms)

new_feature_test_dict=defaultdict(list)

for atoms in tqdm.tqdm_notebook(atom_list_test):
    metals_mask_dict={el:np.array(atoms.get_chemical_symbols())==el for el in ["Al","Ga","In"]}
    nl = NeighborList([2.5/2.]*len(atoms),self_interaction=False, bothways=True,skin=0.0)
    nl.update(atoms)
    vectors=[]
    distances=[]
    for ind in np.arange(len(atoms)):
        indices, offsets = nl.get_neighbors(ind)
        vecs=np.array([atoms.positions[i] + np.dot(offset, atoms.get_cell()) for i, offset in zip(indices, offsets)])-atoms.positions[ind] 
        vectors.append(vecs)
        dists=np.linalg.norm(vecs,axis=1)
        distances.append(dists)   

    vectors=np.array(vectors)
    distances=np.array(distances)


    for el, metals_mask in metals_mask_dict.items():
        dist=list(map(np.mean,distances[metals_mask]))
        new_feature_test_dict[el+"_nn_dist_mean"].append(np.mean(dist)  if len(dist)>0 else 10)
        new_feature_test_dict[el+"_nn_dist_std"].append(np.std(dist)  if len(dist)>0 else 10)
        new_feature_test_dict[el+"_nn_dist_count"].append(np.mean(list(map(len,distances[metals_mask]))))
        centrosym_vecs_norms=[np.linalg.norm(np.array(r).sum(axis=0)) for r in vectors[metals_mask]]
        new_feature_test_dict[el+"_nn_centrosym_mean"].append(np.mean(centrosym_vecs_norms) if len(centrosym_vecs_norms)>0 else 10)
        new_feature_test_dict[el+"_nn_centrosym_std"].append(np.std(centrosym_vecs_norms)  if len(centrosym_vecs_norms)>0 else 10)

    new_feature_test_dict["id"].append(atoms.id)

geom_features_test=pd.DataFrame(new_feature_test_dict)
geom_features_test.to_csv("geom_features_test.csv")
print("Geometry features of test  set saved to geom_features_test.csv.")












