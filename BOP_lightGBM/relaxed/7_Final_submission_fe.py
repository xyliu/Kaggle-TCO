from hyperopt import hp, tpe
from hyperopt.fmin import fmin
import lightgbm
import xgboost

print("LightGBM version",lightgbm.__version__)
print("XGBoost version:",xgboost.__version__)
from lightgbm import LGBMRegressor
from xgboost import XGBRegressor
from helper_repr import *

'''
{'num_leaves': 94, 'n_estimators': 230, 'colsample_bytree': 0.4159857535891618, 'learning_rate': 0.05546884095239686, 'subsample': 0.5645543829581339, 'min_child_weight': 11}
-0.020240888345060985 +/- 0.003019669634288173
'''

'''
lgb_best_params_1= {
    'learning_rate': 0.050031583067449585,
    'min_child_weight': 9,
    'num_leaves': 80,
    'subsample': 0.607446055921953,
    'colsample_bytree': 0.36379689679683647,
    'n_estimators': 165
}
'''




best_features_1 = ['In_nn_dist_mean', 'In_nn_dist_count', 'In_nn_centrosym_mean', 'Al_nn_centrosym_mean', 'Ga_nn_dist_mean', 'In_3_bn_sp_4_std', 'min_ox_dist', 'std_ox_dist', 'In_3_bn_sp_5_mean', 'Al_6_mu_sp_orig_6_mean_gen2', 'percent_atom_in', 'percent_atom_ga', 'lattice_vector_3_ang', 'lattice_vector_1_ang', 'lattice_angle_alpha_degree', 'Al_5_an_sp_1_std_gen2', 'O_3_mu_sp_6_std', 'O_0_bn_sp_2_std', 'In_3_bn_d_4_std', 'Al_6_vdw_radius_diff_std_std_gen2', 'volume_per_atom', 'Al_6_lattice_angle_gamma_degree_mean_gen2', 'Ga_nn_dist_count', 'O_4_mu_d_5_mean', 'Al_4_rp_max_ave_std_mean_gen2', 'O_4_an_d_5_mean', 'In_0_an_sp_2_std_gen2', 'In_4_an_d_5_std', 'In_1_bn_sp_3_mean', 'Al_0_period_diff_std_std_gen2', 'In_4_atomic_radius_rahm_diff_std_std_gen2', 'lattice_angle_beta_degree', 'spacegroup', 'In_4_mu_sp_orig_11_std_gen2', 'Ga_5_EA_diff_std_mean_gen2', 'Al_nn_dist_mean', 'Al_6_heat_of_formation_diff_std_std_gen2', 'Ga_1_mu_sp_2_std', 'O_2_bn_d_2_mean', 'O_0_mu_d_1_mean', 'Ga_4_an_sp_3_mean', 'In_1_an_sp_4_mean_gen2', 'max_ox_dist', 'Al_4_heat_of_formation_diff_mean_std_gen2', 'Al_4_mu_sp_orig_3_std_gen2', 'O_3_rd_max_ave_mean_std_gen2', 'O_5_dipole_polarizability_diff_std_std_gen2', 'Al_4_bn_d_5_mean', 'Al_3_bn_sp_4_std', 'In_2_rp_max_ave_mean_mean_gen2', 'Al_nn_dist_std', 'O_0_mu_d_11_mean', 'In_2_an_sp_2_std', 'O_3_an_d_3_std', 'Al_3_mu_d_2_std', 'Al_6_mass_diff_mean_std_gen2', 'Al_0_an_sp_4_std', 'Al_2_mu_sp_1_std', 'O_3_atomic_radius_rahm_ave_std_std_gen2', 'Ga_1_rs_max_ave_std_mean_gen2', 'In_3_an_sp_5_std_gen2', 'In_2_mu_sp_5_mean', 'Al_4_EA_ave_std_mean_gen2', 'In_2_mu_d_8_std', 'Ga_3_EA_ave_mean_mean_gen2', 'In_3_dipole_polarizability_ave_mean_mean_gen2', 'Ga_2_mu_d_8_std', 'O_4_bn_sp_4_mean_gen2', 'O_1_covalent_radius_ave_mean_std_gen2', 'Al_3_an_sp_1_std', 'volume', 'In_4_atomic_radius_rahm_diff_std_mean_gen2', 'O_6_rd_max_ave_std_mean_gen2', 'In_1_mu_sp_1_std', 'Al_4_electronegativity_ave_std_mean_gen2', 'Al_6_mu_sp_orig_8_std_gen2', 'Al_1_an_sp_5_mean_gen2', 'Ga_3_an_d_1_std', 'O_2_lattice_angle_alpha_degree_mean_gen2', 'Al_1_LUMO_ave_mean_std_gen2', 'O_2_an_sp_5_std', 'O_0_vdw_radius_diff_mean_mean_gen2', 'Ga_4_bn_d_2_std', 'Ga_2_mu_sp_orig_10_std_gen2', 'Al_0_electron_affinity_ave_mean_std_gen2', 'In_5_mu_sp_orig_5_mean_gen2', 'O_3_atomic_radius_rahm_ave_mean_mean_gen2', 'Ga_1_mass_ave_std_std_gen2', 'Ga_0_an_sp_2_mean_gen2', 'In_2_mu_sp_5_std', 'O_3_mu_sp_8_std', 'Ga_3_mu_d_8_std', 'Ga_nn_centrosym_mean', 'O_3_mu_d_2_std', 'In_3_mu_d_10_std', 'Ga_3_mu_sp_orig_7_std_gen2', 'lattice_angle_gamma_degree', 'Ga_5_mu_sp_orig_7_mean_gen2', 'Al_nn_dist_count', 'Al_0_mu_d_10_mean', 'Al_6_rs_max_diff_mean_std_gen2', 'Ga_6_heat_of_formation_diff_mean_mean_gen2', 'In_0_bn_sp_2_std_gen2', 'In_3_atomic_radius_rahm_diff_std_std_gen2', 'Al_6_number_of_total_atoms_std_gen2', 'Al_4_mu_sp_orig_6_std_gen2', 'In_nn_dist_std', 'Ga_6_lattice_vector_1_ang_std_gen2', 'Ga_3_number_of_total_atoms_std_gen2', 'O_1_mu_d_2_mean', 'O_0_bn_sp_6_mean_gen2', 'Al_4_atomic_radius_rahm_diff_mean_mean_gen2', 'Ga_0_mu_sp_orig_4_std_gen2', 'Al_2_bn_d_1_std', 'Ga_4_mass_diff_std_std_gen2', 'O_4_mu_d_5_std', 'In_3_an_sp_5_mean_gen2', 'O_2_HOMO_diff_std_mean_gen2', 'Ga_0_vdw_radius_diff_mean_std_gen2', 'Ga_2_bn_sp_2_std', 'Al_4_bn_d_1_std', 'Al_2_rd_max_ave_mean_mean_gen2', 'Al_4_mu_d_4_std', 'In_3_bn_d_6_std', 'O_1_atomic_volume_diff_std_mean_gen2', 'Al_0_electronegativity_diff_mean_mean_gen2', 'Ga_1_IP_ave_std_std_gen2', 'In_1_vdw_radius_ave_mean_std_gen2', 'ave_oxide_oxide_formation_energy', 'O_4_HOMO_diff_mean_std_gen2', 'In_0_bn_sp_3_std', 'Al_2_bn_d_2_std', 'Al_6_bn_sp_6_mean_gen2', 'O_1_vdw_radius_ave_mean_mean_gen2', 'O_2_bn_sp_1_mean_gen2']

print(best_features_1)
lgb_best_params_1= {
    'num_leaves': 94, 
    'n_estimators': 230, 
    'colsample_bytree': 0.4159857535891618, 
    'learning_rate': 0.05546884095239686, 
    'subsample': 0.5645543829581339, 
    'min_child_weight': 11
}

print(np.shape(np.array(X_total_train.dtypes.index)))
print('O_6_rd_max_ave_std_mean_gen2_gen2' in np.array(X_total_train.dtypes.index))
print(X_total_train['O_6_rd_max_ave_std_mean_gen2'])
print("X_total_train[best_features_1]:", X_total_train[best_features_1])

print("data loaded.\n",flush = True)

lgb=LGBMRegressor(random_state=16,**lgb_best_params_1)

print("next: lgb.fit",flush = True)
lgb.fit(X_total_train[best_features_1],y_train_1)



print("next: lgb.predict",flush = True)
y1_pred_lgb = lgb.predict(X_total_test[best_features_1])

print("y1_pred_lgb:\n", y1_pred_lgb,flush = True)
np.save("pred_fe_test", y1_pred_lgb)

y1_pred_train = lgb.predict(X_total_train[best_features_1])
np.save("pred_fe_train", y1_pred_train)
