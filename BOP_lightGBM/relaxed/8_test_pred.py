import numpy as np
import pandas as pd
print("bandgap:")

print("-test:")
pred_bandgap_test_log1p = np.load("pred_bandgap_test.npy")
print(np.shape(pred_bandgap_test_log1p))
pred_bandgap_test = np.expm1(pred_bandgap_test_log1p)
print(pred_bandgap_test[:10])

print("-train:")
pred_bandgap_train_log1p = np.load("pred_bandgap_train.npy")
print(np.shape(pred_bandgap_train_log1p))
pred_bandgap_train = np.expm1(pred_bandgap_train_log1p)
print(pred_bandgap_train[:10])

print("formation energy:")

print("-test:")
pred_fe_test_log1p = np.load("pred_fe_test.npy")
print(np.shape(pred_fe_test_log1p))
pred_fe_test = np.expm1(pred_fe_test_log1p)
print(pred_fe_test[:10])

print("-train:")
pred_fe_train_log1p = np.load("pred_fe_train.npy")
print(np.shape(pred_fe_train_log1p))
pred_fe_train = np.expm1(pred_fe_train_log1p)
print(pred_fe_train[:10])


#id,formation_energy_ev_natom,bandgap_energy_ev
index_test = np.arange(1, len(pred_bandgap_test)+1, 1)
data_bandgap_test = pd.Series(pred_bandgap_test)
data_fe_test = pd.Series(pred_fe_test)
data_test = pd.DataFrame(index_test, columns = ['id'])
data_test['formation_energy_ev_natom'] = data_fe_test
data_test['bandgap_energy_ev'] = data_bandgap_test

data_test.to_csv("submission_test.csv", header=True, index=False)

index_train = np.arange(1, len(pred_bandgap_train)+1, 1)
data_bandgap_train = pd.Series(pred_bandgap_train)
data_fe_train = pd.Series(pred_fe_train)
data_train = pd.DataFrame(index_train, columns = ['id'])
data_train['formation_energy_ev_natom'] = data_fe_train
data_train['bandgap_energy_ev'] = data_bandgap_train

data_train.to_csv("submission_train.csv", header=True, index=False)
