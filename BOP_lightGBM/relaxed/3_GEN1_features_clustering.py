import pickle
import numpy as np
import tqdm
import pandas as pd

from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import StandardScaler

with open("atoms_train.pckl","rb") as f:
    train_se=pickle.load(f)
    
with open("atoms_test.pckl","rb") as f:
    test_se=pickle.load(f)

print("Train and test pickle files loaded.")

vector_desc_to_include=['an_d', 'bn_d', 'mu_d', 'an_sp', 'bn_sp', 'mu_sp']
scalar_desc_to_include=["volume_d","volume_sp"]

se=train_se[0]

tot_col_names=[col_name+"_"+str(i) for col_name in vector_desc_to_include for i in range(0,len(se._VALUE[col_name][0]))]
tot_col_names+=scalar_desc_to_include

print("len of tot_col_names:", len(tot_col_names))

tot_df_train = pd.DataFrame()
dfs=[tot_df_train]
for se in tqdm.tqdm(train_se):
    df=pd.DataFrame(np.hstack(
        [se._VALUE[desc_name] for desc_name in vector_desc_to_include]
        ))

    for desc in scalar_desc_to_include:
        df[desc] = se._VALUE[desc]
    df["id"]=se._VALUE["id"]
    df["occ"]=se.get_chemical_symbols()
    dfs.append(df)

tot_df_train=pd.concat(dfs)

indices_and_occupation_df_train=tot_df_train[["id","occ"]]
tot_df_train=tot_df_train.drop(["id","occ"],axis=1)

tot_df_test = pd.DataFrame()
dfs=[tot_df_test]
for se in tqdm.tqdm(test_se):
    df=pd.DataFrame(np.hstack(
        [se._VALUE[desc_name] for desc_name in vector_desc_to_include]
        ))

    for desc in scalar_desc_to_include:
        df[desc] = se._VALUE[desc]
    df["id"]=se._VALUE["id"]
    df["occ"]=se.get_chemical_symbols()
    dfs.append(df)

tot_df_test=pd.concat(dfs)

indices_and_occupation_df_test=tot_df_test[["id","occ"]]
tot_df_test=tot_df_test.drop(["id","occ"],axis=1)

tot_df_train.columns = tot_col_names
tot_df_test.columns = tot_col_names

train_len=len(tot_df_train)
test_len=len(tot_df_test)

tot_df=pd.concat([tot_df_train,tot_df_test],axis=0)
indices_and_occupation_df=pd.concat([indices_and_occupation_df_train,indices_and_occupation_df_test],axis=0)

print("Pre-processing finished.")

print("Atomic environment clustering...")

from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.cluster import DBSCAN, SpectralClustering
from sklearn.mixture import GaussianMixture

occ_dict={"Al":0,"Ga":1,"In":2,"O":4}
temp_df =pd.concat([tot_df,indices_and_occupation_df],axis=1)
temp_df["occ_id"]=temp_df["occ"].map(occ_dict)

print("- Oxygen structure map clustering")

temp_df_O = temp_df[temp_df.occ=="O"]

n_clusters_O = 5

clustering_pipeline=make_pipeline(StandardScaler(), KMeans(n_clusters=n_clusters_O,random_state=16))

labels_O=clustering_pipeline.fit_predict(temp_df_O[["an_sp_1","bn_sp_2","volume_sp"]])

print("- Metal structure map clustering")

temp_df_Me = temp_df[temp_df.occ!="O"]
print("temp_df_Me.shape:", temp_df_Me.shape)
n_clusters = 5

print("!!! Unfortunately, random_state was not fixed for this clustering. So, order of cluster labels could not corespond to the cluster labels in final submission. Original data, used in feature selection, model hyperoptimization and final submission are stored in X_structmap_train_short.csv and X_structmap_test_short.csv.")

clustering_pipeline=make_pipeline(StandardScaler(), KMeans(n_clusters=n_clusters,random_state=16))
labels=clustering_pipeline.fit_predict(temp_df_Me[["an_sp_1","bn_sp_2","volume_sp"]])

labels_Me = labels

print("Building structmap dataframe")
mask_O=indices_and_occupation_df.occ=="O"
mask_Me=indices_and_occupation_df.occ!="O"
tot_df.loc[mask_O,"Cluster_label"]=labels_O
tot_df.loc[mask_Me,"Cluster_label"]=labels_Me
tot_df.fillna(-2,inplace=True)
tot_df = tot_df.loc[:,tot_df.var()!=0]
tot_df_train=tot_df.iloc[:train_len,:].copy()
tot_df_test=tot_df.iloc[train_len:,:].copy()
tot_df_train = pd.concat([tot_df_train,indices_and_occupation_df_train],axis=1)
tot_df_test = pd.concat([tot_df_test,indices_and_occupation_df_test],axis=1)
columns_to_aggregate=tot_df_train.columns.drop(['Cluster_label',  'id', 'occ'])

def reorganize_peratom_df(tot_df_original):
    columns_to_aggregate=tot_df_original.columns.drop(['Cluster_label',  'id', 'occ'])
    grpupbyobj=tot_df_original.groupby(["id","occ","Cluster_label"])
    gdf=grpupbyobj[columns_to_aggregate].agg([np.mean,np.std]).fillna(0) #calculate mean and average within each clusters of atoms and structure
    gdf.columns=[n1+"_"+n2 for n1 in gdf.columns.levels[0] for n2 in gdf.columns.levels[1]]
    gdf=gdf.reset_index(["occ","Cluster_label"])
    gdf["Cluster_label"]=gdf["Cluster_label"].astype(int).astype(str)
    gdf["Element_cluster"] = gdf["occ"]+"_"+gdf["Cluster_label"]
    gdf.drop(["occ","Cluster_label"],axis=1,inplace=True)
    ngdf=gdf.pivot(columns="Element_cluster").fillna(0)
    ngdf.columns=[n1+"_"+n2 for n1 in ngdf.columns.levels[1] for n2 in ngdf.columns.levels[0]]
    ngdf = ngdf.loc[:,ngdf.var()!=0]
    return ngdf

X_structmap_train = reorganize_peratom_df(tot_df_train)
train_columns=set(X_structmap_train.columns)
X_structmap_test = reorganize_peratom_df(tot_df_test)
test_columns=set(X_structmap_test.columns)
#check wheter set of columns in train and test is consistent, fill missing columns with zeroes
for col_name in (train_columns - test_columns):
    X_structmap_test[col_name] = 0.
X_structmap_test=X_structmap_test[X_structmap_train.columns]

print("test columns for similarity:", np.all(X_structmap_test.columns==X_structmap_train.columns))

X_structmap_train.index=range(0,len(X_structmap_train))
X_structmap_test.index=range(0,len(X_structmap_test))
X_structmap_train.to_csv("X_structmap_train_short.csv",index=None)
X_structmap_test.to_csv("X_structmap_test_short.csv",index=None)







