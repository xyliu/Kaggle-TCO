import os
import numpy as np
import pandas as pd
import pickle
import tqdm
from sklearn.model_selection import KFold, cross_val_score
from geometrical_features import calculate_geometrical_features_test, calculate_geometrical_features_train, special_xyz_read

cv=KFold(10,shuffle=True,random_state=10)


def rmsle(h, y): 
    """
    Compute the Root Mean Squared Log Error for hypthesis h and targets y

    Args:
        h - numpy array containing predictions with shape (n_samples, n_targets)
        y - numpy array containing targets with shape (n_samples, n_targets)
    """
    #return np.sqrt(np.square(np.log(h + 1) - np.log(y + 1)).mean())	
    return np.sqrt(np.square(h - y ).mean())	

def rmsle_scorer(estimator,X_test,y_test):
    y_pred=estimator.predict(X_test)
    return rmsle(y_test,y_pred)

def neg_rmsle_scorer(estimator,X_test,y_test):
    return -rmsle_scorer(estimator,X_test,y_test)
	

def score_regressor1(clf, train=None,y=None):
    global X_train_1
    if y is None:
        y = y_train_1
    if train is None:
        sc=cross_val_score(clf,X_train_1,y,scoring=neg_rmsle_scorer,cv=cv)
    else:
        sc=cross_val_score(clf,train,y,scoring=neg_rmsle_scorer,cv=cv)
    print(sc.mean(),"+/-",sc.std())
    return sc.mean()

def score_regressor2(clf,train=None,y=None):
    global X_train_2
    if y is None:
        y = y_train_2
    if train is None:
        sc=cross_val_score(clf,X_train_2,y,scoring=neg_rmsle_scorer,cv=cv)
    else:
        sc=cross_val_score(clf,train,y,scoring=neg_rmsle_scorer,cv=cv)
    print(sc.mean(),"+/-",sc.std())
    return sc.mean()
	
def write_predictions(filename, y_pred_1, y_pred_2):
    sample_df=pd.read_csv("sample_submission.csv")
    sample_df["formation_energy_ev_natom"]=np.exp(y_pred_1)-1
    sample_df["bandgap_energy_ev"]=np.exp(y_pred_2)-1
    sample_df.formation_energy_ev_natom[sample_df.formation_energy_ev_natom<0]=0.
    sample_df.to_csv(filename,index=False)

def read_raw_data():
    df = pd.read_csv("train.csv")
    df_test = pd.read_csv("test.csv")
    return df, df_test

def dump_data(fname,data):
    with open(fname,"wb") as f:
        pickle.dump(data,f)

def load_data(fname):
    with open(fname,"rb") as f:
        return pickle.load(f)

def feature_engineering_xyz_data(df, df_test):
    atom_list=[]
    for i in tqdm.tqdm_notebook(df.id):
        atoms=special_xyz_read("train/%d/geometry.xyz"%i)
        atoms.id=i
        atom_list.append(atoms)
    
    atom_list_test=[]
    for i in tqdm.tqdm_notebook(df_test.id):
        atoms=special_xyz_read("test/%d/geometry.xyz"%i)
        atoms.id=i
        atom_list_test.append(atoms)
    
    try:
        ox_dist = load_data("ox_dist.pckl")
    except FileNotFoundError:        
        print("calculating ox_dist")
        ox_dist=np.array([list(map(lambda dist:sorted(dist)[1],atoms.get_all_distances()[atoms.numbers==8])) for atoms in atom_list])
        dump_data("ox_dist.pckl",ox_dist)
        
    try:
        ox_dist_test = load_data("ox_dist_test.pckl")        
    except FileNotFoundError:
        print("calculating ox_dist_test")
        ox_dist_test=np.array([list(map(lambda dist:sorted(dist)[1],atoms.get_all_distances()[atoms.numbers==8])) for atoms in atom_list_test])
        dump_data("ox_dist_test.pckl",ox_dist_test)
    
    vol=[atoms.get_volume() for atoms in atom_list]
    vol_test=[atoms.get_volume() for atoms in atom_list_test]

    df["mean_ox_dist"]=list(map(np.mean,ox_dist))
    df["std_ox_dist"]=list(map(np.std,ox_dist))
    df["min_ox_dist"]=list(map(np.min,ox_dist))
    df["max_ox_dist"]=list(map(np.max,ox_dist))
    df["volume"]=vol
    df["volume_per_atom"]=df["volume"]/df["number_of_total_atoms"]

    df_test["mean_ox_dist"]=list(map(np.mean,ox_dist_test))
    df_test["std_ox_dist"]=list(map(np.std,ox_dist_test))
    df_test["min_ox_dist"]=list(map(np.min,ox_dist_test))
    df_test["max_ox_dist"]=list(map(np.max,ox_dist_test))
    df_test["volume"]=vol_test
    df_test["volume_per_atom"]=df_test["volume"]/df_test["number_of_total_atoms"]
    return df, df_test

# no extra files needed
def feature_engineering_oxide_data(df,df_test):
    # using DFT calculated data for bandgap and formation energy from materialsproject.org
    # elem (Elem-2 O-3) ,SPG,band gap, en.form
    oxide_data=[
        # from https://materialsproject.org/#search/materials/{"nelements"%3A2%2C"elements"%3A"In-O"}
        ["In",167,0.958,1.998],
        ["In",206,0.932,2.027],
        ["In",62,0.112,1.782],

        # from https://materialsproject.org/#search/materials/{"nelements"%3A2%2C"elements"%3A"Al-O"}
        ["Al",12,4.455,3.433],
        ["Al",33,4.827,3.425],
        ["Al",167,5.854,3.442],
        ["Al",208,5.216,3.412],

        # https://materialsproject.org/#search/materials/{"nelements"%3A2%2C"elements"%3A"Ga-O"}
        ["Ga",12,2.008,2.283],
        ["Ga",167,2.402,2.254]
    ]
    oxide_df=pd.DataFrame(oxide_data,columns=["element","spacegroup","oxide_band_gap","oxide_formation_energy"])

    elems=["Al","Ga","In"]
    oxide_props=["oxide_band_gap","oxide_formation_energy"]
    oxide_prop_names=[elem+"_"+prop for prop in oxide_props for elem in elems]
    
    new_df = df.copy()
    for elem in elems:
        elem_oxide_df=oxide_df[oxide_df.element==elem]
        new_df=new_df.merge(elem_oxide_df,on="spacegroup",how="left")
        new_df.drop("element",axis=1,inplace=True)
        new_df=new_df.rename(axis='columns',mapper={"oxide_band_gap":elem+"_oxide_band_gap","oxide_formation_energy":elem+"_oxide_formation_energy"})

    na_dict=new_df[oxide_prop_names].mean().to_dict()
    new_df=new_df.fillna(na_dict)
    for prop in oxide_props:
        new_df["ave_oxide_"+prop]=new_df["percent_atom_al"]*new_df["Al_"+prop]+new_df["percent_atom_ga"]*new_df["Ga_"+prop]+new_df["percent_atom_in"]*new_df["In_"+prop]
    new_df=new_df.drop(oxide_prop_names,axis=1)    
    df=new_df.copy()
    
    new_df = df_test.copy()
    for elem in elems:
        elem_oxide_df=oxide_df[oxide_df.element==elem]
        new_df=new_df.merge(elem_oxide_df,on="spacegroup",how="left")
        new_df.drop("element",axis=1,inplace=True)
        new_df=new_df.rename(axis='columns',mapper={"oxide_band_gap":elem+"_oxide_band_gap","oxide_formation_energy":elem+"_oxide_formation_energy"})

    new_df=new_df.fillna(na_dict)
    for prop in oxide_props:
        new_df["ave_oxide_"+prop]=new_df["percent_atom_al"]*new_df["Al_"+prop]+new_df["percent_atom_ga"]*new_df["Ga_"+prop]+new_df["percent_atom_in"]*new_df["In_"+prop]
    new_df=new_df.drop(oxide_prop_names,axis=1)
    df_test=new_df.copy()
    
    return df,df_test

#geom_features_train.csv, geom_features_test.csv
def feature_engineering_atomic_geometry_data(df,df_test):

    #use cached results otherwise recalculate
    if not os.path.isfile("geom_features_train.csv"):
        calculate_geometrical_features_train()

    # use cached results otherwise recalculate
    if not os.path.isfile("geom_features_test.csv"):
        calculate_geometrical_features_test()

    geom_features_train = pd.read_csv("geom_features_train.csv")
    geom_features_test = pd.read_csv("geom_features_test.csv")
    df=pd.merge(df,geom_features_train,on='id')
    df_test=pd.merge(df_test,geom_features_test,on='id')
    return df,df_test


#if __name__=="__main__":
####main part

#use train.csv, test.csv
df,df_test = read_raw_data()

# no extra files
df,df_test = feature_engineering_oxide_data(df,df_test)

# ox_dist.pckl / check if not exist
df, df_test =feature_engineering_xyz_data( df,df_test)

#geom_features_train.csv, geom_features_test.csv / Final_submission_geom_feature_engineering
df,df_test = feature_engineering_atomic_geometry_data(df,df_test)

X_train = df.drop(["id","formation_energy_ev_natom","bandgap_energy_ev"],axis=1)
X_test = df_test.drop(["id"],axis=1)

y_train_1 = np.log(df["formation_energy_ev_natom"]+1)
y_train_2 = np.log(df["bandgap_energy_ev"]+1)

# 1st features generation
# normalized structuremap data
# GEN1_structmap_moments_calculations.ipynb  / GEN1_features_clustering.ipynb 
X_structmap_train=pd.read_csv("X_structmap_train_short.csv")
X_structmap_test=pd.read_csv("X_structmap_test_short.csv")

# 2nd features generation
# nonnormalized structuremap data and atomic-related properties
# GEN2_features_generation.ipynb / GEN2_features_clustering.ipynb
X_moments_atomic_train = pd.read_csv("X_moments_atomic_train.csv")
X_moments_atomic_test = pd.read_csv("X_moments_atomic_test.csv")
X_moments_atomic_train.columns=X_moments_atomic_train.columns+"_gen2"
X_moments_atomic_test.columns=X_moments_atomic_test.columns+"_gen2"

X_total_train=pd.concat((X_train,X_structmap_train,X_moments_atomic_train),axis=1)
X_total_test=pd.concat((X_test,X_structmap_test,X_moments_atomic_test),axis=1)
