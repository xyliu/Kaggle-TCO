import numpy as np
import tqdm
import pickle
from geometrical_features import read_test_xyz_files,read_train_xyz_files

def get_strucmap_props(struct, orbital='d', store_norm_cells=True):
    mymap = StructureMap(
        atoms=[struct],
        geo_eval=False,
        moments=12,
        store_anbn=True,
        store_norm_cells=store_norm_cells,
        orbital=orbital)

    #print("mymap:", mymap)
    mymap.evaluate_strucs()
    mus = np.array(mymap.mu)[0]
    ans = np.array(mymap.an)[0]
    bns = np.array(mymap.bn)[0]
    #print("mus, ans, bns got.")
    if store_norm_cells:
        ncells = mymap.norm_cells[0]
        volumes = np.array(list(map(np.linalg.det, ncells)))
        return ans, bns, mus, volumes
    return ans, bns, mus
atoms_train = read_train_xyz_files()
atoms_test = read_test_xyz_files()
print("atoms_train, atoms_test read.\natoms_train:",np.shape(atoms_train), "atoms_test:", np.shape(atoms_test)) 


from bopcat.structmap import StructureMap
print("Structuremap imported.")

for atoms in tqdm.tqdm(atoms_train):
    atoms._VALUE=dict()
    atoms._VALUE["id"]=atoms.id
    
    an_d,bn_d,mu_d,volume_d = get_strucmap_props(atoms,orbital="d")
    an_sp,bn_sp,mu_sp,volume_sp = get_strucmap_props(atoms,orbital="sp")
    
    atoms._VALUE["an_d"]=an_d
    atoms._VALUE["bn_d"]=bn_d
    atoms._VALUE["mu_d"]=mu_d
    atoms._VALUE["volume_d"]=volume_d
    
    atoms._VALUE["an_sp"]=an_sp
    atoms._VALUE["bn_sp"]=bn_sp
    atoms._VALUE["mu_sp"]=mu_sp
    atoms._VALUE["volume_sp"]=volume_sp

for atoms in tqdm.tqdm(atoms_test):
    atoms._VALUE=dict()
    atoms._VALUE["id"]=atoms.id
    
    an_d,bn_d,mu_d,volume_d = get_strucmap_props(atoms,orbital="d")
    an_sp,bn_sp,mu_sp,volume_sp = get_strucmap_props(atoms,orbital="sp")
    
    atoms._VALUE["an_d"]=an_d
    atoms._VALUE["bn_d"]=bn_d
    atoms._VALUE["mu_d"]=mu_d
    atoms._VALUE["volume_d"]=volume_d
    
    atoms._VALUE["an_sp"]=an_sp
    atoms._VALUE["bn_sp"]=bn_sp
    atoms._VALUE["mu_sp"]=mu_sp
    atoms._VALUE["volume_sp"]=volume_sp

with open("atoms_train.pckl","wb") as f:
    pickle.dump(atoms_train,f)

with open("atoms_test.pckl","wb") as f:
    pickle.dump(atoms_test,f)
