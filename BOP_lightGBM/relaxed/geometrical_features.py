import pandas as pd
import tqdm
import numpy as np
from collections import defaultdict
from ase.neighborlist import NeighborList
from ase.atoms import Atoms

def special_xyz_read(fname):
    """
    Read special XYZ format from Kaggle Nomad 2018 competition and converts to ASE Atoms object
    """
    with open(fname) as f:
        lat_vecs=[]
        coords=[]
        elems=[]
        for l in f:
            if l.startswith("#"):
                continue
            elif l.startswith("lattice_vector"):
                lat_vec=list(map(float,l.split()[1:]))
                lat_vecs.append(lat_vec)
            elif l.startswith("atom"):
                lsplt=l.split()
                coord=list(map(float,lsplt[1:4]))
                elem=lsplt[-1]
                coords.append(coord)
                elems.append(elem)

    atoms=Atoms(symbols=elems,positions=coords,cell=lat_vecs,pbc=True)
    return atoms



def read_train_xyz_files():
    atom_list = []
    for i in range(1, 2401):
        atoms = special_xyz_read("train/%d/geometry.xyz" % i)
        atoms.id = i
        atom_list.append(atoms)
    return atom_list


def read_test_xyz_files():
    atom_list_test = []
    for i in range(1, 601):
        atoms = special_xyz_read("test/%d/geometry.xyz" % i)
        atoms.id = i
        atom_list_test.append(atoms)
    return atom_list_test


def calculate_geometrical_features_train(train_csv="geom_features_train.csv"):
    # read raw xyz in ASE Atoms format
    atom_list = read_train_xyz_files()
    new_feature_dict=defaultdict(list)

    for atoms in tqdm.tqdm(atom_list):
        metals_mask_dict={el:np.array(atoms.get_chemical_symbols())==el for el in ["Al","Ga","In"]}
        nl = NeighborList([2.5/2.]*len(atoms),self_interaction=False, bothways=True,skin=0.0)
        nl.update(atoms)
        vectors=[]
        distances=[]
        for ind in np.arange(len(atoms)):
            indices, offsets = nl.get_neighbors(ind)
            vecs=np.array([atoms.positions[i] + np.dot(offset, atoms.get_cell()) for i, offset in zip(indices, offsets)])-atoms.positions[ind]
            vectors.append(vecs)
            dists=np.linalg.norm(vecs,axis=1)
            distances.append(dists)

        vectors=np.array(vectors)
        distances=np.array(distances)


        for el, metals_mask in metals_mask_dict.items():
            dist=list(map(np.mean,distances[metals_mask]))
            new_feature_dict[el+"_nn_dist_mean"].append(np.mean(dist)  if len(dist)>0 else 10)
            new_feature_dict[el+"_nn_dist_std"].append(np.std(dist)  if len(dist)>0 else 10)
            new_feature_dict[el+"_nn_dist_count"].append(np.mean(list(map(len,distances[metals_mask]))))
            centrosym_vecs_norms=[np.linalg.norm(np.array(r).sum(axis=0)) for r in vectors[metals_mask]]
            new_feature_dict[el+"_nn_centrosym_mean"].append(np.mean(centrosym_vecs_norms) if len(centrosym_vecs_norms)>0 else 10)
            new_feature_dict[el+"_nn_centrosym_std"].append(np.std(centrosym_vecs_norms)  if len(centrosym_vecs_norms)>0 else 10)

        new_feature_dict["id"].append(atoms.id)
    geom_features=pd.DataFrame(new_feature_dict)
    geom_features=geom_features.fillna(0)
    geom_features.to_csv(train_csv)


def calculate_geometrical_features_test(test_csv="geom_features_test.csv"):
    # Test data
    atom_list_test = read_test_xyz_files()
    new_feature_test_dict=defaultdict(list)
    for atoms in tqdm.tqdm_notebook(atom_list_test):
        metals_mask_dict={el:np.array(atoms.get_chemical_symbols())==el for el in ["Al","Ga","In"]}
        nl = NeighborList([2.5/2.]*len(atoms),self_interaction=False, bothways=True,skin=0.0)
        nl.update(atoms)
        vectors=[]
        distances=[]
        for ind in np.arange(len(atoms)):
            indices, offsets = nl.get_neighbors(ind)
            vecs=np.array([atoms.positions[i] + np.dot(offset, atoms.get_cell()) for i, offset in zip(indices, offsets)])-atoms.positions[ind]
            vectors.append(vecs)
            dists=np.linalg.norm(vecs,axis=1)
            distances.append(dists)

        vectors=np.array(vectors)
        distances=np.array(distances)


        for el, metals_mask in metals_mask_dict.items():
            dist=list(map(np.mean,distances[metals_mask]))
            new_feature_test_dict[el+"_nn_dist_mean"].append(np.mean(dist)  if len(dist)>0 else 10)
            new_feature_test_dict[el+"_nn_dist_std"].append(np.std(dist)  if len(dist)>0 else 10)
            new_feature_test_dict[el+"_nn_dist_count"].append(np.mean(list(map(len,distances[metals_mask]))))
            centrosym_vecs_norms=[np.linalg.norm(np.array(r).sum(axis=0)) for r in vectors[metals_mask]]
            new_feature_test_dict[el+"_nn_centrosym_mean"].append(np.mean(centrosym_vecs_norms) if len(centrosym_vecs_norms)>0 else 10)
            new_feature_test_dict[el+"_nn_centrosym_std"].append(np.std(centrosym_vecs_norms)  if len(centrosym_vecs_norms)>0 else 10)

        new_feature_test_dict["id"].append(atoms.id)
    geom_features_test=pd.DataFrame(new_feature_test_dict)
    geom_features_test.to_csv(test_csv)