from hyperopt import hp, tpe
from hyperopt.fmin import fmin
import lightgbm
import xgboost

print("LightGBM version",lightgbm.__version__)
print("XGBoost version:",xgboost.__version__)
from lightgbm import LGBMRegressor
from xgboost import XGBRegressor
from helper_repr import *


best_features_2 = ['volume_per_atom', 'lattice_vector_3_ang', 'ave_oxide_oxide_formation_energy', 'Al_nn_centrosym_mean', 'In_nn_centrosym_mean', 'ave_oxide_oxide_band_gap', 'In_nn_dist_count', 'In_4_an_d_5_std', 'In_1_mu_sp_1_mean', 'Al_6_covalent_radius_ave_mean_mean_gen2', 'Al_nn_dist_mean', 'percent_atom_al', 'min_ox_dist', 'mean_ox_dist', 'Ga_nn_centrosym_mean', 'In_4_atomic_volume_ave_mean_mean_gen2', 'Al_3_mu_d_3_mean', 'In_4_covalent_radius_ave_std_std_gen2', 'O_4_an_sp_5_std_gen2', 'O_4_an_sp_2_mean_gen2', 'spacegroup', 'lattice_angle_beta_degree', 'Al_2_period_ave_mean_std_gen2', 'Al_5_dipole_polarizability_diff_std_std_gen2', 'In_1_mu_d_3_mean', 'Al_2_mu_sp_7_mean', 'Al_0_electronegativity_diff_mean_mean_gen2', 'Al_5_mu_sp_orig_11_std_gen2', 'O_4_rs_max_diff_std_mean_gen2', 'In_6_mu_sp_orig_6_mean_gen2', 'In_1_an_sp_2_mean', 'In_5_mu_sp_orig_1_std_gen2', 'Al_4_rs_max_diff_std_std_gen2', 'Al_3_mu_d_11_std', 'In_3_an_sp_4_mean', 'Ga_1_an_sp_2_mean', 'Ga_1_bn_sp_6_std', 'O_1_atomic_volume_diff_std_mean_gen2', 'max_ox_dist', 'Al_4_mu_sp_2_mean', 'O_5_atomic_volume_diff_mean_mean_gen2', 'Al_5_vdw_radius_ave_mean_std_gen2', 'Al_4_atomic_radius_rahm_diff_mean_mean_gen2', 'percent_atom_in', 'O_4_rd_max_ave_mean_std_gen2', 'Al_1_mu_d_9_mean', 'In_3_an_sp_1_std', 'In_4_an_sp_1_std', 'O_1_vdw_radius_diff_mean_std_gen2', 'Ga_1_electronegativity_ave_mean_std_gen2', 'Al_4_mu_d_6_std', 'Al_4_mu_sp_orig_8_mean_gen2', 'Al_6_lattice_angle_gamma_degree_mean_gen2', 'O_2_electron_affinity_ave_mean_std_gen2', 'Al_3_bn_sp_6_std', 'Al_6_rp_max_ave_mean_std_gen2', 'Al_4_bn_sp_3_mean', 'Ga_0_an_sp_2_mean_gen2', 'O_1_an_d_5_std', 'Al_0_bn_sp_2_std_gen2', 'O_6_an_sp_4_std_gen2', 'Al_6_atomic_volume_diff_std_std_gen2', 'O_4_bn_sp_1_std_gen2', 'Al_5_percent_atom_al_std_gen2', 'O_4_mu_d_4_std', 'Ga_nn_dist_mean', 'Al_0_mu_sp_orig_1_std_gen2', 'Al_3_mu_d_5_std', 'Al_5_lattice_angle_gamma_degree_std_gen2', 'O_4_an_sp_4_mean_gen2', 'O_1_bn_d_5_std', 'O_0_lattice_vector_2_ang_mean_gen2', 'Al_1_mu_sp_2_std', 'Al_3_bn_d_1_std', 'Al_0_period_ave_std_mean_gen2', 'Al_6_rs_max_diff_mean_std_gen2', 'Al_1_rs_max_diff_mean_std_gen2', 'Al_0_an_sp_1_std', 'In_5_atomic_volume_ave_std_std_gen2', 'Ga_3_mu_d_10_mean', 'Al_0_bn_d_5_mean', 'Ga_6_EA_ave_std_mean_gen2', 'Al_6_mu_sp_orig_2_mean_gen2', 'Ga_0_an_sp_2_std_gen2', 'In_4_heat_of_formation_diff_mean_std_gen2', 'Ga_1_IP_ave_mean_std_gen2', 'Ga_5_electron_affinity_ave_std_mean_gen2', 'O_0_bn_sp_6_mean_gen2', 'Al_2_mass_diff_mean_mean_gen2', 'Ga_6_IP_ave_mean_mean_gen2', 'std_ox_dist', 'Al_0_lattice_angle_beta_degree_mean_gen2', 'Ga_1_an_sp_2_std', 'Al_1_mu_sp_7_std', 'Al_0_electron_affinity_diff_std_mean_gen2', 'O_5_LUMO_diff_mean_mean_gen2', 'Al_1_bn_sp_3_std', 'Ga_0_mu_sp_orig_2_std_gen2', 'O_5_atomic_volume_ave_std_std_gen2', 'Al_0_mu_sp_1_mean', 'In_1_number_of_total_atoms_std_gen2', 'Al_0_mass_ave_std_mean_gen2', 'In_1_bn_d_6_std', 'Ga_2_mu_sp_6_std', 'Al_2_bn_d_1_mean', 'O_2_covalent_radius_diff_std_std_gen2', 'Al_0_bn_d_1_mean', 'In_3_atomic_volume_ave_mean_mean_gen2', 'Ga_6_mu_sp_orig_10_mean_gen2', 'Ga_nn_dist_count', 'lattice_angle_alpha_degree', 'Al_1_mu_d_4_std', 'In_0_bn_d_1_std', 'Al_1_an_sp_5_mean', 'Al_0_mu_sp_5_std', 'In_5_IP_ave_std_mean_gen2', 'Al_0_atomic_radius_rahm_ave_mean_std_gen2', 'In_4_bn_d_2_std', 'In_0_mu_d_6_std', 'In_2_mu_d_7_std', 'Ga_0_HOMO_ave_mean_std_gen2', 'Al_5_dipole_polarizability_ave_mean_mean_gen2', 'Ga_2_bn_sp_1_mean', 'O_4_mu_d_9_mean', 'In_2_mu_sp_4_std', 'Al_0_EA_ave_mean_mean_gen2', 'Al_1_an_d_5_std', 'In_5_mu_sp_orig_4_std_gen2', 'Al_1_dipole_polarizability_ave_mean_std_gen2', 'In_0_rs_max_ave_mean_std_gen2', 'Al_3_bn_d_4_std', 'Ga_2_bn_sp_2_std', 'In_4_mu_sp_orig_9_mean_gen2', 'Al_3_bn_d_3_std', 'In_3_mu_sp_3_mean', 'Ga_0_mu_sp_orig_3_std_gen2', 'Al_1_rd_max_ave_std_std_gen2', 'In_5_mu_sp_orig_3_mean_gen2', 'Al_3_bn_sp_4_std', 'Al_5_EA_ave_mean_std_gen2', 'Ga_5_HOMO_ave_std_std_gen2', 'Ga_5_LUMO_ave_std_std_gen2', 'Al_5_EA_ave_mean_mean_gen2', 'In_3_covalent_radius_diff_mean_std_gen2', 'Al_4_rp_max_diff_std_std_gen2', 'In_3_heat_of_formation_diff_mean_std_gen2', 'In_4_percent_atom_al_std_gen2', 'In_3_atomic_radius_rahm_diff_mean_mean_gen2', 'Ga_2_bn_d_2_std', 'In_3_an_d_3_std', 'O_1_atomic_volume_diff_mean_std_gen2', 'O_4_mu_d_11_std', 'Ga_6_rs_max_ave_std_mean_gen2', 'Al_0_EA_diff_std_std_gen2', 'In_nn_dist_std', 'In_2_mu_sp_orig_5_std_gen2', 'O_3_bn_d_3_std', 'In_4_HOMO_diff_mean_std_gen2', 'O_3_mu_sp_10_std', 'In_3_mu_d_1_mean', 'In_3_bn_d_6_std', 'O_3_lattice_angle_gamma_degree_std_gen2', 'Ga_6_mass_diff_mean_std_gen2', 'Al_4_bn_d_2_std', 'Ga_0_mu_sp_7_std', 'O_3_mass_ave_std_mean_gen2', 'Al_6_LUMO_diff_std_std_gen2', 'Al_nn_centrosym_std', 'Al_6_mu_sp_orig_8_std_gen2', 'Al_3_mu_d_10_std', 'In_3_vdw_radius_diff_mean_std_gen2', 'Al_5_an_sp_1_std_gen2', 'Al_1_an_d_1_std', 'Al_0_mu_sp_8_std', 'In_4_mu_sp_orig_1_mean_gen2', 'O_2_dipole_polarizability_diff_std_std_gen2', 'In_1_mu_sp_orig_1_std_gen2', 'Al_4_IP_diff_mean_std_gen2', 'In_6_an_sp_4_mean_gen2', 'O_2_bn_sp_1_mean', 'Al_4_HOMO_ave_std_std_gen2', 'In_2_mu_sp_1_std', 'O_3_atomic_radius_rahm_ave_std_std_gen2', 'lattice_angle_gamma_degree', 'Ga_0_mu_sp_4_std', 'Al_0_mu_sp_11_mean', 'Ga_0_an_sp_1_mean_gen2', 'In_3_bn_sp_2_std', 'In_4_dipole_polarizability_diff_mean_mean_gen2', 'Al_6_dipole_polarizability_diff_std_std_gen2', 'In_0_an_d_4_mean', 'O_1_mu_sp_orig_10_mean_gen2', 'Al_2_mu_sp_6_std', 'Ga_0_electron_affinity_diff_mean_std_gen2']


'''
{'num_leaves': 100, 'n_estimators': 205, 'colsample_bytree': 0.557092919609042, 'learning_rate': 0.06321284363398724, 'subsample': 0.6115668990676426, 'min_child_weight': 11}
-0.06650732370510301 +/- 0.0091024322527026
'''

'''
best_params_lgb_2 = {
    'learning_rate': 0.05000743610479569,
    'min_child_weight': 7,
    'num_leaves': 98,
    'subsample': 0.3613801787750081,
    'colsample_bytree': 0.3449012379174325,
    'n_estimators': 205
}
'''
best_params_lgb_2 = {
    'num_leaves': 100, 
    'n_estimators': 205, 
    'colsample_bytree': 0.557092919609042, 
    'learning_rate': 0.06321284363398724, 
    'subsample': 0.6115668990676426, 
    'min_child_weight': 11
}

lgb2=LGBMRegressor(n_jobs=-1,**best_params_lgb_2,random_state=16)

print("data loaded.\nnext: score_regressor2",flush = True)
score_regressor2(lgb2,X_total_train[best_features_2])

print("next: lgb2.fit",flush = True)
lgb2.fit(X_total_train[best_features_2],y_train_2)

print(y_train_2)

print("next: lgb2.predict",flush = True)
y2_pred_lgb = lgb2.predict(X_total_test[best_features_2])

print("y2_pred_lgb:\n", y2_pred_lgb,flush = True)
#np.save("pred_bandgap_test", y2_pred_lgb)

y2_pred_train = lgb2.predict(X_total_train[best_features_2])
#np.save("pred_bandgap_train", y2_pred_train)
