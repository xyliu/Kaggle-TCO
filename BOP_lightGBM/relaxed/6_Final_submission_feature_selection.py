import matplotlib.pyplot as plt
plt.switch_backend('agg')
from hyperopt import hp, tpe
from hyperopt.fmin import fmin
import lightgbm
import xgboost
print("LightGBM version",lightgbm.__version__)
print("XGBoost version:",xgboost.__version__)
from lightgbm import LGBMRegressor
from xgboost import XGBRegressor
from helper_repr import *
X_moments_atomic_train = pd.read_csv("X_moments_atomic_train.csv")
X_moments_atomic_test = pd.read_csv("X_moments_atomic_test.csv")
X_moments_atomic_train.columns=X_moments_atomic_train.columns+"_gen2"
X_moments_atomic_test.columns=X_moments_atomic_test.columns+"_gen2"
X_structmap_train=pd.read_csv("X_structmap_train_short.csv")
X_structmap_test=pd.read_csv("X_structmap_test_short.csv")
print("X_structmap_train.shape:", X_structmap_train.shape)
print("X_structmap_test.shape:", X_structmap_test.shape)

X_total_train=pd.concat((X_train,X_structmap_train,X_moments_atomic_train),axis=1)
X_total_test=pd.concat((X_test,X_structmap_test,X_moments_atomic_test),axis=1)
print("X_total_train.shape:", X_total_train.shape)
print("X_total_test.shape:",X_total_test.shape)

xgb=XGBRegressor(seed=16)

lgb = LGBMRegressor(random_state=16,n_estimators=100)

print("Formation energy:")
bxgb = XGBRegressor(seed=16,n_estimators=25)
bxgb.fit(X_total_train,y_train_1)

bst=bxgb.booster()
fi_2_df=pd.DataFrame({k:[v] for k,v in bst.get_fscore().items()}).T.reset_index()
fi_2_df.columns=["feature","imp"]
fi_2_df=fi_2_df.sort_values("imp",ascending=False)
features=[]
scores=[]
for n_estimators in range(30,250,1):
    print("N-estimator = ",n_estimators)
    bxgb = XGBRegressor(seed=16,n_estimators=n_estimators)
    bxgb.fit(X_total_train,y_train_1)
    bst=bxgb.booster()
    fi_2_df=pd.DataFrame({k:[v] for k,v in bst.get_fscore().items()}).T.reset_index()
    fi_2_df.columns=["feature","imp"]
    fi_2_df=fi_2_df.sort_values("imp",ascending=False)
    imp_features=fi_2_df[fi_2_df.imp>1e-7].feature.tolist()
    print("Num of features:",len(imp_features))
    sc=score_regressor1(xgb,X_total_train[imp_features])
    features.append(imp_features)
    scores.append(sc)


plt.plot(range(30,250,1),scores)
plt.savefig("6_fe_plot.png")
plt.scatter(range(30,250,1),scores)
plt.savefig("6_fe_scatter.png")

print("max(scores):", max(scores))

best_features_1 = features[np.argmax(scores)]
print("best_features_1:", best_features_1)

print("hyperoptimization of LightGBM on best features...")
def objective_lgb(params):
    params = {"num_leaves":int(params["num_leaves"]),
              "n_estimators":int(params["n_estimators"]),
             "colsample_bytree":params["colsample_bytree"],
              "learning_rate":params["learning_rate"],
              "subsample":params["subsample"],              
             "min_child_weight":int(params["min_child_weight"])}
    print(params)
    reg=LGBMRegressor(n_jobs=-1,**params,random_state=16)
    sc = score_regressor1(reg,X_total_train[best_features_1])
    return -sc
space = {
    'num_leaves':hp.quniform('num_leaves',80,100,2),
    'colsample_bytree':hp.uniform('colsample_bytree',0.3,1.0),
    'subsample':hp.uniform('subsample',0.3,1.0),
    'min_child_weight':hp.quniform('min_child_weight',5,11,1),
    'n_estimators':hp.quniform('n_estimators',25,250,5),
    'learning_rate':hp.uniform('learning_rage',0.05,0.1),
}
best = fmin(fn=objective_lgb,space=space,algo=tpe.suggest, max_evals=250)

print("Best:", best)


print("Bandgap:")


