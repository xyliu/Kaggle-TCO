#%pylab inline
import pandas as pd
import numpy as np
import pickle
import tqdm
from tqdm import tqdm_notebook
from geometrical_features import read_test_xyz_files,read_train_xyz_files
atoms_train = read_train_xyz_files()
atoms_test = read_test_xyz_files()
with open("atoms_train.pckl","rb") as f:
    train_se=pickle.load(f)
with open("atoms_test.pckl","rb") as f:
    test_se=pickle.load(f)

print("Train and test pckl read.")

#from ase.calculators.bopfox import BOPfox
from bopcat.bopfox import BOPfox

calc = BOPfox(version='bop',
                      savelog=False,
                      printmu_averaged=True, printmu=True,
                      printbonds=True,                      
                      task='energy',
                      scfsteps=0, efermisteps=0,
                      ignore_errors=False,
                      model='test', 
                      modelsbx="models_gen2.bx",#! d model, afterall :(, but still works
                      printanbn=True
                      )

print("BOPfox calc generated.")

print("Looping over tqdm_notebook(train_se)")    
print("len of train_se:", len(train_se))

#for atoms in tqdm_notebook(train_se):
for atoms in tqdm.tqdm(train_se):
    atoms_W = atoms.copy()
    atoms_W.set_chemical_symbols(["W"]*len(atoms)) # replace all elements with tungsten and use tungsten bond integrals from "models_gen2.bx"
    calc.set_atoms(atoms_W)
    calc.calculate()
    atoms_moments=[calc.get_moments(i,moment='all') for i in range(len(atoms_W))]
    atoms._VALUE["mu_sp_orig"] = atoms_moments

print("Looping over tqdm_notebook(test_se)")
for atoms in tqdm.tqdm(test_se):
#for atoms in tqdm_notebook(test_se):
    atoms_W = atoms.copy()
    atoms_W.set_chemical_symbols(["W"]*len(atoms)) # replace all elements with tungsten and use tungsten bond integrals from "models_gen2.bx"
    calc.set_atoms(atoms_W)
    calc.calculate()
    atoms_moments=[calc.get_moments(i,moment='all') for i in range(len(atoms_W))]
    atoms._VALUE["mu_sp_orig"] = atoms_moments
    
print("Loops done.")


from mendeleev import element
from collections import defaultdict
import pandas as pd
import seaborn.apionly as sns

mendeleev_prop_names = [
    'atomic_radius_rahm',
    'atomic_volume',
    'covalent_radius',
    'dipole_polarizability',
    'electron_affinity',
    'heat_of_formation',
    'period',   
    'vdw_radius',
]

properties = defaultdict(defaultdict)

print("Looping elems")
elems = ["Al","In","Ga","O"]
for elem in tqdm.tqdm(elems):
    el = element(elem)
    for prop_name in mendeleev_prop_names:
        properties[prop_name][elem]=getattr(el,prop_name)
        
prop_name_csv = [
    "EA", "HOMO", "IP", "LUMO", "mass", "electronegativity", "rd_max",
    "rp_max", "rs_max"
]
print("Looping prop_name")
for prop_name in tqdm.tqdm(prop_name_csv):
    df=pd.read_csv(prop_name+".csv",header=None,index_col=0)
    for elem in elems:        
            properties[prop_name][elem]=df.loc[elem,1]
            
properties_df = pd.DataFrame(properties)

print("properties_df:",properties_df.head())

properties_df.to_csv("atomic_properties.csv")

atomistic_properties_name = properties_df.columns.tolist()

print("Atomic neighbours:")

from ase.neighborlist import NeighborList
cutoff_dist_dict=properties_df["rp_max"].to_dict()
desc_name = [prop_name+suffix+agg_type for prop_name in atomistic_properties_name for agg_type in ["_mean","_std"] for suffix in ["_diff","_ave"]  ]
def generate_arithmetic_descriptors(valA,valB,dist,chA,chB):
    smooth_factor = np.exp(-dist/(cutoff_dist_dict[chA]+cutoff_dist_dict[chB]))
    return [
        (valA-valB),
        (valA+valB)/2, 
        smooth_factor
    ]
def calculate_atomic_descriptors(atoms):
    
    
    chem_symbs=np.array(atoms.get_chemical_symbols())
    nl = NeighborList(list(map(lambda a:cutoff_dist_dict[a],chem_symbs)),self_interaction=False,bothways=True)
    nl.build(atoms)

    #loop by each atom in structure
    structure_descriptors=[]
    for at_ind in range(len(atoms)):
        #print(at_ind)
        selfatom = atoms[at_ind]
        r0 = selfatom.position
        self_ch = selfatom.symbol
        #loop by properties
        atom_descriptors=[]
        for prop_name in atomistic_properties_name:
            self_prop = properties_df.loc[self_ch, prop_name]
            indices, offsets = nl.get_neighbors(at_ind)
            rs = np.array([
                atoms.positions[i] + np.dot(offset, atoms.get_cell())
                for i, offset in zip(indices, offsets)
            ])

            dists = np.linalg.norm(rs - r0, axis=1)
            chs = chem_symbs[indices]
            #loop and average by neighbours
            desc=[generate_arithmetic_descriptors(properties_df.loc[
                            ch, prop_name], self_prop, dist, ch, self_ch)
                        for ch, dist in zip(chs, dists)]
            desc=np.array(desc)
            means = [ np.sum(desc[:,i]*desc[:,-1])/np.sum(desc[:,-1]) for i in range(desc.shape[1]-1)]
            stds=[np.sqrt(np.sum((desc[:,i]-means[i])**2*desc[:,-1])/np.sum(desc[:,-1])) for i in range(len(means))]
            atom_descriptors += means
            atom_descriptors += stds
        structure_descriptors.append(atom_descriptors)
    atoms_df=pd.DataFrame(structure_descriptors,columns=desc_name)
    return atoms_df

print(len(desc_name))

print("Looping over train..")
for atoms in tqdm.tqdm(train_se):
    atoms_df=calculate_atomic_descriptors(atoms)
    for col_name in atoms_df.columns:
        atoms._VALUE[col_name]=atoms_df[col_name].values

print("Looping over test..")
for atoms in tqdm.tqdm(test_se):
    atoms_df=calculate_atomic_descriptors(atoms)
    for col_name in atoms_df.columns:
        atoms._VALUE[col_name]=atoms_df[col_name].values

print("Writing to pckl..")
with open("atoms_train.pckl","wb") as f:
    pickle.dump(train_se,f)
with open("atoms_test.pckl","wb") as f:
    pickle.dump(test_se,f)
    
print("Done.")





