
"""
This module is the BOPfit interface to the structure map.
It can be used to calculate distances between different structures and to
visualize structures on structure maps.
Beside the coordinates in the structure map, geometrical quantities are
calculated.
It is recommended to use BOPfox rev 141 or higher.

It is written by:
Alvin Noe Ladines
Jan Jenke
"""

import numpy as np
import matplotlib.pyplot as pl
import itertools

#from bopcat import bopfox as bopcalc
from bopcat import bopcal as bopcalc

from bopcat import bopio
from bopcat.bopmodel import modelsbx
from bopcat.bopmodel import atomsbx
from bopcat.bopmodel import bondsbx



class StructureMap:
    def __init__(self, **kwargs):
        # atoms can be either a list of ase atoms objects or a hdf5 file
        self.atoms = None
        # coordinates can be provided either as a list or a hdf5 file
        # if coordinates are not provided, they will be calculated
        self.coordinates = []
        self.mu = []
        self.an = []
        self.bn = []
        self.norm_cells = []
        self.ave_mu = []
        self.local_ave_mu = []
        self.bonds = []
        self.dists = []
        self.neighbors = []
        self.min_dist = []
        self.pf = []
        self.dim = []
        self.int_coord = []
        self.frac_coord = []
        self.labels = []
        self.set_defaults()
        self.has_coordinates = False
        self.a_start = 1.

        for key in kwargs:
            if key.lower() == 'atoms':
                self.atoms = kwargs[key]
            elif key.lower() == 'coordinates':
                self.coordinates = kwargs[key]
                self.has_coordinates = True
            elif key.lower() == 'modelsbx':
                self.modelsbx = kwargs[key]
            # not needed
            elif key.lower() == 'mu':
                self.mu = kwargs[key]
            elif key.lower() == 'an':
                self.an = kwargs[key]
            elif key.lower() == 'bn':
                self.bn = kwargs[key]
            elif key.lower() == 'norm_cells':
                self.norm_cells = kwargs[key]
            elif key.lower() == 'ave_mu':
                self.ave_mu = kwargs[key]
            elif key.lower() == 'local_ave_mu':
                self.local_ave_mu = kwargs[key]
            elif key.lower() == 'bonds':
                self.bonds = kwargs[key]
            elif key.lower() == 'dists':
                self.dists = kwargs[key]
            elif key.lower() == 'neighbors':
                self.neighbors = kwargs[key]
            elif key.lower() == 'min_dist':
                self.min_dist = kwargs[key]
            elif key.lower() == 'pf':
                self.pf = kwargs[key]
            elif key.lower() == 'int_coord':
                self.int_coord = kwargs[key]
            elif key.lower() == 'frac_coord':
                self.frac_coord = kwargs[key]
            elif key.lower() == 'labels':
                self.labels = kwargs[key]
            elif key.lower() == 'orbital':
                self.orbital = kwargs[key]
            elif key.lower() == 'moments':
                self.moments = kwargs[key]
            elif key.lower() == 'mem_limit':
                self.mem_limit = kwargs[key]
            elif key.lower() == 'print_labels_from_info':
                self.print_labels_from_info = kwargs[key]
            elif key.lower() == 'enumerate_atoms':
                self.enumerate_atoms = kwargs[key]
            elif key.lower() == 'geo_eval':
                self.geo_eval = kwargs[key]
            elif key.lower() == 'store_moments':
                self.store_moments = kwargs[key]
            elif key.lower() == 'store_anbn':
                self.store_anbn = kwargs[key]
            elif key.lower() == 'store_norm_cells':
                self.store_norm_cells = kwargs[key]
            elif key.lower() == 'atoms_to_eval':
                self.atoms_to_eval = kwargs[key]
            elif key.lower() == 'structures_to_eval':
                self.structures_to_eval = kwargs[key]
            elif key.lower() == 'rcut':
                self.rcut = kwargs[key]
            elif key.lower() == 'dcut':
                self.dcut = kwargs[key]
            elif key.lower() == 'a_start':
                self.a_start = kwargs[key]
            else:
                print("Unrecognized keyword %s" % key)

        self.i_evaluated = len(self.coordinates)

    def update(self, **kwargs):
        """
        This function may be used to update / set several attributes of the
        StructureMap object
        :param kwargs:
        :return:
        """

        had_coordinates = self.has_coordinates
        has_new_coordinates = False
        has_new_atoms = False

        for key in kwargs:
            if key.lower() == 'atoms':
                for i in range(len(kwargs[key])):
                    self.atoms.append(kwargs[key][i])
                    has_new_atoms = True
            if key.lower() == 'coordinates':
                for i in range(len(kwargs[key])):
                    self.coordinates.append(kwargs[key][i])
                    has_new_coordinates = True
                if had_coordinates:
                    self.has_coordinates = True
            if key.lower() == 'mu':
                for i in range(len(kwargs[key])):
                    self.mu.append(kwargs[key][i])
            if key.lower() == 'ave_mu':
                for i in range(len(kwargs[key])):
                    self.ave_mu.append(kwargs[key][i])
            if key.lower() == 'bonds':
                for i in range(len(kwargs[key])):
                    self.bonds.append(kwargs[key][i])
            if key.lower() == 'dists':
                for i in range(len(kwargs[key])):
                    self.dists.append(kwargs[key][i])
            if key.lower() == 'neighbors':
                for i in range(len(kwargs[key])):
                    self.neighbors.append(kwargs[key][i])
            if key.lower() == 'min_dist':
                for i in range(len(kwargs[key])):
                    self.min_dist.append(kwargs[key][i])
            if key.lower() == 'pf':
                for i in range(len(kwargs[key])):
                    self.pf.append(kwargs[key][i])
            if key.lower() == 'int_coord':
                for i in range(len(kwargs[key])):
                    self.int_coord.append(kwargs[key][i])
            if key.lower() == 'frac_coord':
                for i in range(len(kwargs[key])):
                    self.frac_coord.append(kwargs[key][i])
            if key.lower() == 'labels':
                for i in range(len(kwargs[key])):
                    self.labels.append(kwargs[key][i])
            if key.lower() == 'orbital':
                print('WARNING: orbital cannot be updated, current ' \
                      'orbital type is: ', self.orbital)
            if key.lower() == 'moments':
                print('WARNING: moments cannot be updated, current ' \
                      'number of moments is: ', self.moments)
            if key.lower() == 'mem_limit':
                self.mem_limit = kwargs[key]
            if key.lower() == 'print_labels_from_info':
                self.print_labels_from_info = kwargs[key]
            if key.lower() == 'enumerate_atoms':
                self.enumerate_atoms = kwargs[key]
            if key.lower() == 'geo_eval':
                self.geo_eval = kwargs[key]
            if key.lower() == 'store_moments':
                self.store_moments = kwargs[key]
            if key.lower() == 'store_anbn':
                self.store_anbn = kwargs[key]
            if key.lower() == 'store_norm_cells':
                self.store_norm_cells = kwargs[key]
            if key.lower() == 'atoms_to_eval':
                self.atoms_to_eval = kwargs[key]
            if key.lower() == 'structures_to_eval':
                self.structures_to_eval = kwargs[key]
            if key.lower() == 'rcut':
                self.rcut = kwargs[key]
            if key.lower() == 'dcut':
                self.dcut = kwargs[key]

        if has_new_atoms and (not has_new_coordinates):
            self.has_coordinates = False

        if not self.has_coordinates:
            self.evaluate_strucs()
        else:
            self.i_evaluated = len(self.coordinates)

    def set_defaults(self):
        """
        This function sets the standard default choices.
        It can be invoked if you unpickle a map created with old/incomplete
        defaults
        :return:
        """
        self.orbital = 'd'
        self.geo_eval = True
        self.store_moments = True
        self.store_anbn = False
        self.store_norm_cells = False
        self.atoms_to_eval = 'all'
        self.structures_to_eval = 'all'
        # The user may provide a memory limit for the BOPfox calculation
        # This is useful when evaluating very inhomogeneous structures
        # because a scaling in the volume may result in
        # an unhandable number of bonds which will fill the whole memory.
        # Those strutures need to be rescaled anyway.
        # This is automatically done when normalizing the second moment
        self.mem_limit = 2000000
        # Four moments are enough for the structure map, however more can
        # be evaluated
        self.moments = 4
        self.rcut = 3.27390638523
        self.dcut = 0.35
        self.has_coordinates = False
        # If you want to print labels from the info dictionary of the atom
        # objects, make use of this flag
        self.print_labels_from_info = False
        self.enumerate_atoms = False
        self.i_evaluated = 0

    def gen_hist(self, struc_id=None, n_figure=None, show_hist=True,
                 filename=None):
        """
        This function can generate histograms for all structures in the map
        :param struc_id: int: (n-th atoms object),
                         list of integers: list of number of the atoms
                         object list of strings: searches for the labels,
                         if they exist
                         None: Create all histograms
        :param n_figure: Specify the number of the matplotlib figure
        :param show_hist: Show the histogram?
        :param filename: If provided: Save the histogram with the specified
                         filename
        :return:
        """

        if type(n_figure) == int:
            pl.figure(n_figure)

        if type(struc_id) == int:
            ids = [struc_id]
            subplot_x = 0
            subplot_y = 0
        elif type(struc_id) == list or type(struc_id) == tuple or \
                type(struc_id) == np.ndarray:
            n_struc = len(struc_id)
            ids = []
            for id_i in range(n_struc):
                if type(struc_id[id_i]) == int or \
                        type(struc_id[id_i]) == float:
                    ids.append(int(struc_id[id_i]))
                elif type(struc_id[id_i]) == str:
                    label_matches = \
                        np.argwhere(np.array(self.labels)
                                    == struc_id[id_i])
                    n_matches = len(label_matches)
                    if n_matches > 0:
                        for match_i in range(n_matches):
                            ids.append(label_matches[match_i][0])
                    else:
                        print('WARNING: Got unknown value in ID')
            n_struc = len(ids)
            subplot_x = int(n_struc**0.5)
            subplot_y = subplot_x + 1
            if subplot_x*subplot_y < n_struc:
                subplot_x += 1
        elif struc_id is None:
            n_struc = len(self.atoms)
            ids = list(range(n_struc))
            subplot_x = int(n_struc**0.5)
            subplot_y = subplot_x + 1
            if subplot_x*subplot_y < n_struc:
                subplot_x += 1
        else:
            print('Unknown input for ID')
            return

        if self.bonds == []:
            print('Bonds were not evaluated. No histogram can be plotted!')
            return

        print('ids = ', ids)

        for curr_id_i, curr_id in enumerate(ids):
            pl.subplot(subplot_x, subplot_y, curr_id_i+1)
            for atom_i in range(len(self.dists[curr_id])):
                x, y = np.histogram(self.dists[curr_id][atom_i][atom_i],
                                    range=(0, self.rcut+1), bins=100)
                width = y[1]-y[0]
                x = x/float(sum(x))
                pl.bar(height=x, left=y[1:], width=width)
            if not (self.labels == []):
                pl.title(self.labels[curr_id])
            pl.plot(np.arange(0, self.rcut+1, 0.001),
                    _f_cut(np.arange(0, self.rcut+1, 0.001),
                           self.rcut, self.dcut),
                    color='r')

        if show_hist:
            pl.show()

        if type(filename) == str:
            pl.savefig(filename)

    def set_plot_task(self, xcoord='mu_3', ycoord='mu_4'):

        plot_task = {'coord_x': [self.coordinates, 0, False, r'$a^{(1)}$'],
                     'coord_y': [self.coordinates, 1, False, r'$b^{(2)}$'],
                     'mu_3': [self.mu, 2, False, r'$\mu_3$'],
                     'mu_4': [self.mu, 3, False, r'$\mu_4$'],
                     'mu_5': [self.mu, 4, False, r'$\mu_5$'],
                     'mu_6': [self.mu, 5, False, r'$\mu_6$'],
                     'mu_7': [self.mu, 6, False, r'$\mu_7$'],
                     'mu_8': [self.mu, 7, False, r'$\mu_8$'],
                     'mu_9': [self.mu, 8, False, r'$\mu_9$'],
                     'a_1': [self.an, 1, False, r'$a^{(1)}$'],
                     'b_2': [self.bn, 2, False, r'$b^{(2)}$'],
                     'mu_4': [self.mu, 3, False, r'$\mu_4$'],
                     'local_ave_mu_3': [self.local_ave_mu, 2, False,
                                        r'$\langle \mu_3 \rangle_{loc}$'],
                     'local_ave_mu_4': [self.local_ave_mu, 3, False,
                                        r'$\langle \mu_4 \rangle_{loc}$'],
                     'local_ave_mu_5': [self.local_ave_mu, 4, False,
                                        r'$\langle \mu_5 \rangle_{loc}$'],
                     'local_ave_mu_6': [self.local_ave_mu, 5, False,
                                        r'$\langle \mu_6 \rangle_{loc}$'],
                     'local_ave_mu_7': [self.local_ave_mu, 6, False,
                                        r'$\langle \mu_7 \rangle_{loc}$'],
                     'local_ave_mu_8': [self.local_ave_mu, 7, False,
                                        r'$\langle \mu_8 \rangle_{loc}$'],
                     'local_ave_mu_9': [self.local_ave_mu, 8, False,
                                        r'$\langle \mu_9 \rangle_{loc}$'],
                     'ave_mu_3': [self.ave_mu, 2, True,
                                  r'$\langle \mu_3 \rangle_{loc}$'],
                     'ave_mu_4': [self.ave_mu, 3, True,
                                  r'$\langle \mu_4 \rangle_{loc}$'],
                     'ave_mu_5': [self.ave_mu, 4, True,
                                  r'$\langle \mu_5 \rangle_{loc}$'],
                     'ave_mu_6': [self.ave_mu, 5, True,
                                  r'$\langle \mu_6 \rangle_{loc}$'],
                     'ave_mu_7': [self.ave_mu, 6, True,
                                  r'$\langle \mu_7 \rangle_{loc}$'],
                     'ave_mu_8': [self.ave_mu, 7, True,
                                  r'$\langle \mu_8 \rangle_{loc}$'],
                     'ave_mu_9': [self.ave_mu, 8, True,
                                  r'$\langle \mu_9 \rangle_{loc}$']}

        if xcoord in list(plot_task.keys()):
            array_to_plot_x, index_x, global_coords_x, labels_x\
                = plot_task[xcoord]
        else:
            print(xcoord, 'is a unknown plotting task')
            raise

        if ycoord in list(plot_task.keys()):
            array_to_plot_y, index_y, global_coords_y, labels_y\
                = plot_task[ycoord]
        else:
            print(ycoord, 'is a unknown plotting task')
            raise

        if global_coords_x != global_coords_y:
            print('Global and local coordinates cannot be mixed')
            raise
        else:
            global_coords = global_coords_x

        return array_to_plot_x, array_to_plot_y, index_x, index_y, \
               labels_x, labels_y, global_coords

    def plot_schematic_map(self, n_figure=None, areas=[]):

        fig = pl.figure(n_figure)

        for area in areas:
            try:
                x = area["x"]
                y_lower = area["y_lower"]
                y_upper = area["y_upper"]
            except:
                print('Not x, y_lower and y_upper provided')
            hatch = area["hatch"]
            color = area["color"]
            edgecolor = area["edgecolor"]
            pl.fill_between(x, y_lower, y_upper, hatch=hatch, color=color,
                            edgecolor=edgecolor)

    def plot_map(self, n_figure=None, show_map=True, color='k',
                 colormap=None, colormap_values=None,
                 show_right_colorbar=False, show_top_colorbar=False,
                 has_right_colorbar=False, has_top_colorbar=False,
                 title_right_colorbar='', title_top_colorbar='',
                 global_colormap=None, label=None, legend = None,
                 xcoord='coord_x', ycoord='coord_y', print_labels=True,
                 filename=None, point_size=40, title=None, vmin=0,
                 vmax=1, colorbar_ticks=None, markers=None, \
                 xytext_values=None, update_legend=True,
                 legend_lines=[], legend_title=None,
                 ncol_legend=2, annotate_fontsize=20,
                 subplots_adjust=True,
                 plotter=pl):
        """
        Generate two dimensional structure map and identify coordinates
        for input atoms. The order parameters should be normalized to 1.
        :param n_figure: Specify the number of the matplotlib figure
        :param show_map: Show the map?
        :param color: Color of the points
        :param label: Labels
        :param xcoord: Quantity on the x-axis
        :param ycoord: Quantity on the y-axis
        :param print_labels: Print labels?
        :param filename: If provided: Save a image of the map with the
        provided filename
        :return:
        """
        # TODO: Reduce usage of self.atoms as much as possible

        # mode "paper" or "talk"
        mode = "paper"

        if type(n_figure) == int:
            fig = plotter.figure(n_figure)

        array_to_plot_x, array_to_plot_y, index_x, index_y, labels_x, \
        labels_y, global_coords = self.set_plot_task(xcoord, ycoord)

        if markers == None:
            maker_list = ['o']*len(array_to_plot_x)
        else:
            maker_list = markers[:]

        if type(legend) == str or legend == None:
            legend_arr = [legend] * len(array_to_plot_x)
        elif type(legend) == list or type(legend) == np.ndarray:
            legend_arr = legend
        else:
            print('Datatype for legend is unknown')
            NotImplementedError()

        if global_colormap == True:
            print_color = 'global'
        elif global_colormap == False:
            print_color = 'local'
        else:
            print_color = 'same'

        if self.atoms is None:
            return None

        if type(n_figure) == int:
            plotter.figure(n_figure)

        if self.print_labels_from_info:
            name = 'system_ID'
            self.labels = ['']*len(self.atoms)
            for i in range(len(self.atoms)):
                if name in self.atoms[i].info:
                    self.labels[i] = self.atoms[i].info[name]
                else:
                    print('No %s found for Atoms %d. '%(name,i))
                    print('Unable to print label.')
            print_labels = True

        if not self.print_labels_from_info:
            coord0 = []
            coord1 = []

            if print_labels:
                # Check if the provided labels exist and make sense
                if len(self.labels) == len(self.atoms):
                    print_labels = True
                else:
                    print_labels = False
                    if not (self.labels == []):
                        print('Labels are of wrong shape. Points in the ' \
                              'structure map are not labeled!')
            try:
                plotter.subplot(111)
            except:
                print('plotter has no subplot')
            if (not print_labels) and (not self.enumerate_atoms) \
                and (colormap_values == None):
                # i structure index
                # j atom index
                for i, coord in enumerate(array_to_plot_x):

                    coord0 = []
                    coord1 = []

                    n_atoms = self.atoms[i].get_number_of_atoms()
                    if not global_coords:
                        for j in range(n_atoms):
                            coord0.append(array_to_plot_x[i][j][index_x])
                            coord1.append(array_to_plot_y[i][j][index_y])
                    else:
                        coord0.append(array_to_plot_x[i][index_x])
                        coord1.append(array_to_plot_y[i][index_y])

                    if type(color)==str:
                        new_color = color
                    elif type(color)==list:
                        new_color = color[i]

                    line = plotter.scatter(coord0, coord1, c=new_color,
                               s=point_size, label=legend_arr[i],
                               edgecolors='face', marker=maker_list[i],
                                      picker=5)
                    legend_lines.append(line)
            else:
                for i, coord in enumerate(array_to_plot_x):
                    n_atoms = self.atoms[i].get_number_of_atoms()
                    if not global_coords:
                        for j in range(n_atoms):
                            coord0 = array_to_plot_x[i][j][index_x]
                            coord1 = array_to_plot_y[i][j][index_y]
                            if print_color == 'local':
                                new_color = colormap_values[i][j][j]
                            elif print_color == 'global':
                                new_color = colormap_values[i]
                            else:
                                new_color = color
                            if j == 0:
                                new_label = legend_arr[i]
                            else:
                                new_label = None

                            if type(maker_list[i]) == list:
                                marker = maker_list[i][j]
                            else:
                                marker = maker_list[i]
                            line = plotter.scatter(coord0, coord1,
                                       c=new_color,
                                       cmap=pl.cm.get_cmap(colormap),
                                       vmin=vmin, vmax=vmax,
                                       s=point_size,
                                       label=new_label,
                                       edgecolors='face',
                                       marker = marker, picker=5)
                            legend_lines.append(line)
                            txt = ''
                            if print_labels:
                                txt = self.labels[i]
                            if self.enumerate_atoms:
                                txt = txt + ' ' + str(j)
                            if txt != '':
                                if xytext_values != None:
                                    if type(xytext_values[i][2]) != list:
                                        xytext_values_i = xytext_values[i]
                                    else:
                                        xytext_values_i = \
                                            xytext_values[i][j]

                                    if xytext_values_i[2] == None:
                                        arrowprops = None
                                    else:
                                        arrowprops = dict(
                                            facecolor='black',
                                            shrink=0.01,
                                            width=.3,
                                            headwidth=5.,
                                            headlength=5.)
                                    plotter.annotate(txt,
                                    (coord0, coord1),
                                    xytext=(coord0+xytext_values_i[0],
                                            coord1+xytext_values_i[1]),
                                    ha="right", va="center",
                                    arrowprops=arrowprops,
                                    fontsize=annotate_fontsize)
                                else:
                                    plotter.annotate(txt, (coord0, coord1),
                                                fontsize=annotate_fontsize,
                                                xytext=(coord0 ,#+ 0.005,
                                                        coord1))
                    else:
                        print('global_coords')
                        coord0 = array_to_plot_x[i][index_x]
                        coord1 = array_to_plot_y[i][index_y]
                        if print_color == 'local':
                            print('WARNING: Cannot print local colors ' \
                                  'for global coordinates')
                            new_color = 'k'
                        elif print_color == 'global':
                            new_color = colormap_values[i]
                        else:
                            new_color = color
                        line = plotter.scatter(coord0, coord1,
                                   c=new_color,
                                   cmap=plotter.cm.get_cmap(colormap),
                                   vmin=vmin, vmax=vmax,
                                   s=point_size,
                                   label=legend_arr[i],
                                   edgecolors='face',
                                   marker=maker_list[i], picker=5)
                        legend_lines.append(line)
                        txt = ''
                        if print_labels:
                            txt = self.labels[i]
                        if txt != '':
                            if xytext_values[i][2] == None:
                                arrowprops = None
                            else:
                                arrowprops = dict(
                                    facecolor='black',
                                    shrink=0.01,
                                    width=.3,
                                    headwidth=5.,
                                    headlength=5.)
                            plotter.annotate(txt, (coord0, coord1),
                                        arrowprops=arrowprops,
                                        xytext=(coord0+xytext_values[i][0],
                                        coord1+xytext_values[i][1]),
                                        fontsize=annotate_fontsize)

            try:
                plotter.xlabel(labels_x, fontsize=10)
                plotter.ylabel(labels_y, fontsize=10)
                plotter.xticks(fontsize=10)
                plotter.yticks(fontsize=10)
            except:
                print('Failed to set labels and ticks')
            if type(filename) == str:
                plotter.savefig(filename)

            if title != None:
                plotter.title(title)

            left_plot = 0.08
            width_colorbar = 0.03
            if show_top_colorbar or has_top_colorbar:
                top = 0.85
                left_plot = 0.14
            else:
                top = 0.95

            if show_right_colorbar:
                if mode == "talk":
                    bottom = 0.28
                    right_plot = 0.65
                else:
                    bottom = 0.25
                    right_plot = 0.9
                left_colorbar = right_plot + 0.01
                # [left, bottom, width, height]
                cbaxes = fig.add_axes([left_colorbar, bottom,
                                       width_colorbar, top-bottom])
                cbaxes.tick_params(labelsize=10)
                colorbar = plotter.colorbar(cax=cbaxes)
                colorbar.set_label(label=title_right_colorbar, size=10)
                if colorbar_ticks == None:
                    colorbar.set_ticks(np.linspace(vmin, vmax, 6))
                else:
                    colorbar.set_ticks(colorbar_ticks)
            if show_top_colorbar:
                if mode == "talk":
                    bottom = 0.28
                    right_plot = 0.65
                else:
                    bottom = 0.1
                    right_plot = 0.98
                left_colorbar = right_plot + 0.01
                left_colorbar = left_plot
                # [left, bottom, width, height]
                cbaxes = fig.add_axes([left_colorbar, top+0.01,
                                       right_plot - left_plot,
                                       width_colorbar])
                cbaxes.tick_params(labelsize=10, left=True)
                colorbar = plotter.colorbar(cax=cbaxes,
                                       orientation='horizontal')
                colorbar.ax.xaxis.set_ticks_position('top')
                colorbar.ax.xaxis.set_label_position('top')
                if colorbar_ticks == None:
                    colorbar.set_ticks(np.linspace(vmin, vmax, 6))
                else:
                    colorbar.set_ticks(colorbar_ticks)
                colorbar.set_label(label=title_top_colorbar, size=10)

            bottom = 0.1
            right_plot = 0.98

            if has_right_colorbar:
                if mode == "talk":
                    bottom = 0.28
                    right_plot = 0.65
                else:
                    bottom = 0.25
                    right_plot = 0.9
            if has_top_colorbar:
                if mode == "talk":
                    bottom = 0.28
                    right_plot = 0.65
                else:
                    bottom = 0.1
                    right_plot = 0.98

            if subplots_adjust:
                plotter.subplots_adjust(left=left_plot, right=right_plot, top=top,
                                   bottom=bottom)
            if show_map:
                plotter.show()

            if mode == "talk":
                loc = 2,
                bbox_to_anchor = (0.7, 0.85)
            else:
                # loc = 2,
                # bbox_to_anchor = (0.7, 0.85)
                loc = 1
                bbox_to_anchor = (right_plot, 0.16)

            if legend!=None:
                if update_legend:
                    print('update')
                    legend = plotter.legend(fontsize=10,
                                       loc=loc,
                                       scatterpoints=1,
                                       ncol=ncol_legend, columnspacing=0,
                                       borderpad=0.2,
                                       labelspacing=0, handletextpad=0,
                                       borderaxespad=0.,
                                       title=legend_title,
                                       bbox_to_anchor=bbox_to_anchor,
                                       bbox_transform=pl.gcf().transFigure
                                       )
                    if legend_title!=None:
                        plotter.setp(legend.get_title(), fontsize=10)
                    legendHandles = plotter.gca().get_legend().legendHandles
                    for i in range(len(legendHandles)):
                        if type(color) == str:
                            new_color = color
                        elif type(color) == list:
                            new_color = color[i]
                        else:
                            new_color = 'black'
                        legendHandles[i].set_color(new_color)
                else:
                    if mode == "talk":
                        loc = 2
                        bbox_to_anchor = (0.7, 0.85)
                    else:
                        loc = 1
                        bbox_to_anchor = (right_plot, 0.16)
                        # loc = 2
                        # bbox_to_anchor = (left_plot, 0.20)
                    # Create a legend for the first line.
                    print('else')
                    legend = plotter.legend(handles=legend_lines,
                                       loc=loc,
                                       fontsize=10, scatterpoints=1,
                                       ncol=ncol_legend, columnspacing=0,
                                       borderpad=0.2, labelspacing=0,
                                       handletextpad=0,
                                       borderaxespad=0.,
                                       title=legend_title,
                                       bbox_to_anchor=bbox_to_anchor,
                                       bbox_transform=pl.gcf().transFigure)
                    if legend_title != None:
                        plotter.setp(legend.get_title(), fontsize=10)
                    # Add the legend manually to the current Axes.
                    legendHandles = plotter.gca().get_legend().legendHandles
                    print(len(legendHandles))
                    for i in range(len(legendHandles)):
                        if type(color) == str:
                            new_color = color
                        elif type(color) == list:
                            new_color = color[i]
                        else:
                            new_color = 'black'
                        legendHandles[i].set_color(new_color)
                    plotter.gca().add_artist(legend)

        else:
            # TODO: Add the possibility to print labels from the info
            # dictionary of each atoms object
            return None

    def gen_path_in_map(self, struc_id, color='k', n_figure=None,
                        show_map=True, start_label=None, end_label=None,
                        label=None, linewidth=2, xcoord='coord_x',
                        ycoord='coord_y', filename=None, annotate=False,
                        annotate_txt=None, show_line=True,
                        update_legend=True, legend_lines=[],
                        legend_title=None, ncol_legend=1, plotter=pl):
        """
        Print a path in the map
        :param struc_id: Id of the atoms object
        :param color: Color of the path in the map
        :param n_figure: number of matplot figure
        :param show_map: Show the map?
        :param start_label: Do not plot points until this label is found
        (TODO: Add the possibility to use ids)
        :param end_label: Stop plotting points, when this label is found
        :param label: If provided, the label occurs in the legend
        :param linewidth: Linewidth of the path
        :param xcoord: Quantity on the x-axis
        :param ycoord: Quantity on the y-axis
        :param filename: If provided: Save a image of the map with the
        provided filename
        :return:
        """

        # mode "paper" or "talk"
        mode = "paper"

        if self.atoms is None:
            return None

        if type(n_figure) == int:
            plotter.figure(n_figure)

        array_to_plot_x, array_to_plot_y, index_x, index_y, labels_x, \
        labels_y, global_coords = self.set_plot_task(xcoord, ycoord)

        n_coords = len(array_to_plot_x)

        coord0 = [None]*n_coords
        coord1 = [None]*n_coords

        if (start_label is not None) and (end_label is not None):
            print_path = False
            for i in range(n_coords):
                if self.labels[i] == start_label:
                    print_path = True
                if self.labels[i] == end_label:
                    print_path = False
                if print_path:
                    coord0[i] = array_to_plot_x[i][struc_id][index_x]
                    coord1[i] = array_to_plot_y[i][struc_id][index_y]
        else:
            for i in range(n_coords):
                coord0[i] = array_to_plot_x[i][struc_id][index_x]
                coord1[i] = array_to_plot_y[i][struc_id][index_y]

        if label is None:
            if show_line:
                line, = plotter.plot(coord0, coord1, color=color,
                               linewidth=linewidth)
                legend_lines.append(line)
            if annotate:
                for i in range(len(coord0)):
                    if annotate_txt == None:
                        txt = str(i)
                    else:
                        txt = str(annotate_txt)
                    plotter.annotate(txt, xy=(coord0[i], coord1[i]))
        else:
            if show_line:
                line, = plotter.plot(coord0, coord1, color=color,
                                linewidth=linewidth, label=label)
                legend_lines.append(line)

        left_plot = 0.08

        if update_legend:
            if mode == "talk":
                bbox_to_anchor = (0.7, 0.85)
            else:
                bbox_to_anchor = (left_plot, 0.16)
            # Create a legend for the first line.
            legend = plotter.legend(handles=legend_lines,
                               loc=2,
                               title=legend_title,
                               bbox_to_anchor=bbox_to_anchor,
                               bbox_transform=plotter.gcf().transFigure,
                               ncol=ncol_legend, columnspacing=0,
                               borderpad=0.2, labelspacing=0,
                               handletextpad=0,
                               borderaxespad=0., fontsize=10
                               )
            plotter.setp(legend.get_title(), fontsize=10)
            # Add the legend manually to the current Axes.
            plotter.gca().add_artist(legend)

        try:
            plotter.xlabel(labels_x)
            plotter.ylabel(labels_y)
        except:
            print('Failed to set labels')
        if show_map:
            plotter.show()

        if type(filename) == str:
            plotter.savefig(filename)

    def get_atomi(self, struc):
        if len(struc.split('/')) == 5:
            stype = 'system_ID'
        else:
            stype = 'strucname'
        out = None
        for i in range(len(self.atoms)):
            # try if struc is ID
                if struc == self.atoms[i].info[stype]:
                    out = i
                    break
        return out

    def get_coordinate(self, p):
        if isinstance(p, int):
            pass
        elif isinstance(p, str):
            p = self.get_atomi(p)
        out = np.array(self.coordinates[p])
        return out

    def get_distance(self, p1, p2):
        """
        p1/p2 can either be atom index, system_ID or tuple of
        """
        if isinstance(p1, tuple):
            c1 = np.array(p1)
        else:
            c1 = self.get_coordinate(p1)
        if isinstance(p1, tuple):
            c2 = np.array(p2)
        else:
            c2 = self.get_coordinate(p2)
        d = c1 - c2
        return np.linalg.norm(d)

    def sample(self, radius=0.2, center=(0.5, 0.5), natoms=10):
        """
        determine natoms atoms from structure map within radius of distance
        from center
        """
        inside = []
        for i in range(len(self.atoms)):
            d = self.get_distance(center, i)
            if d <= radius:
                inside.append(i)
        if len(inside) < natoms:
            print('only %d atoms inside circle(r=%2.3f).' % \
                  (len(inside), radius))
            print('try increasing radius')
            natoms = len(inside)
        inside = np.random.choice(inside, natoms, replace=False)
        inatoms = []
        for i in inside:
            inatoms.append(self.atoms[i])
        return inatoms

    def normalize_coordinates(self):
        coor = np.array(self.coordinates)
        minx = 1E99
        miny = 1E99
        maxx = -1E99
        maxy = -1E99
        for i in range(len(coor)):
            xy = np.amin(coor[i],axis=0)
            if xy[0] < minx:
                minx = xy[0]
            if xy[1] < miny:
                miny = xy[1]
            xy = np.amax(coor[i],axis=0)
            if xy[0] > maxx:
                maxx = xy[0]
            if xy[1] > maxy:
                maxy = xy[1]
        for i in range(len(coor)):
            c = np.array(coor[i])
            dx = (c.T[0] - minx)/float(maxx-minx)
            dy = (c.T[1] - miny)/float(maxy-miny)
            coor[i] = list(np.array([dx,dy]).T)
        self.coordinates = coor

    def evaluate_strucs(self, n_mom=None, rcut=None, dcut=None,
                        r2cut=None, d2cut=None):
        """
        Evaluate the structures. Heart of the evaluation
        :return:
        """
        #print("In: evaluate_strucs")
        # _write_atomsbx(self.orbital)
        # _write_bondsbx(self.orbital)
        if rcut==None:
            n_mom = self.moments
        if rcut==None:
            rcut = self.rcut
        if dcut==None:
            dcut = self.dcut
        if r2cut==None:
            r2cut = self.rcut
        if d2cut==None:
            d2cut = self.dcut

        bopatoms = _write_atomsbx(self.orbital)
        bopbonds = _write_bondsbx(self.orbital, rcut=rcut, dcut=dcut,
                                  r2cut=rcut, d2cut=dcut)
        #print("bopatoms:", bopatoms)
        model = modelsbx(model='test', atomsbx=[bopatoms],
                         bondsbx=[bopbonds],
                         infox_parameters={'moments': n_mom,
                                           'bandwidth': 'azero',
                                           'nexpmoments': 0})

        model.write()
        #print("model:", model)
        if self.structures_to_eval=='all':
            #print("In:self.structures_to_eval=='all'")
            for atoms_i in self.atoms[self.i_evaluated:]:
                #print("atoms_i:", atoms_i)
                # For a given structure only certain atoms can be evaluated
                if type(self.atoms_to_eval) == list \
                    or type(self.atoms_to_eval) == np.ndarray or \
                        type(self.atoms_to_eval) == tuple:
                    atoms_to_eval = self.atoms_to_eval[atoms_i]
                else:
                    atoms_to_eval = self.atoms_to_eval

                #print("atoms_to_eval:", atoms_to_eval)
                mu, an, bn, bonds, dists, neighbors, min_dist, pf, dim, \
                int_coord, frac_coord, norm_cells = \
                    _evaluate_atoms(atoms_i, self.orbital, self.rcut,
                                    self.dcut, n_mom=self.moments,
                                    mem_limit=self.mem_limit,
                                    geo_eval=self.geo_eval,
                                    atoms_to_eval=atoms_to_eval,
                                    a_start=self.a_start,
                                    store_norm_cells=self.store_norm_cells)
                #print("mu, an:", mu, an)
                if self.store_moments:
                    self.mu.append(mu)

                if self.store_anbn:
                    self.an.append(an)
                    self.bn.append(bn)

                if self.store_norm_cells:
                    self.norm_cells.append(norm_cells)

                if self.geo_eval:
                    self.bonds.append(bonds)
                    self.dists.append(dists)
                    self.neighbors.append(neighbors)
                    self.min_dist.append(min_dist)
                    self.pf.append(pf)
                    self.dim.append(dim)
                    self.int_coord.append(int_coord)
                    self.frac_coord.append(frac_coord)

                new_coordinates = []
                for i in range(len(mu)):
                    new_coordinates.append([an[i][1], bn[i][2]])
                self.coordinates.append(new_coordinates)

        self.i_evaluated = len(self.atoms)
        self.has_coordinates = True
        print("Done evaluate_strucs.")


    def get_coordinates(self):
        """
        User function to get the coordinates
        :return:
        """
        return self.coordinates

    def get_averaged_moments(self):
        """
        User function to get and evaluate the (globally) averaged moments
        :return:
        """
        try:
            self.ave_mu
        except ValueError:
            self.ave_mu = []
        if not (self.ave_mu == []):
            return self.ave_mu
        else:
            # Calculate averaged moments
            n_struc = len(self.mu)
            self.ave_mu = [None]*n_struc
            for i_struc in range(n_struc):
                n_atoms = len(self.mu[i_struc])
                n_mom = len(self.mu[i_struc][0])
                self.ave_mu[i_struc] = [None]*n_mom
                for i_mom in range(n_mom):
                    ave_val = 0.
                    for i_atom in range(n_atoms):
                        ave_val = ave_val + self.mu[i_struc][i_atom][i_mom]
                    ave_val /= n_atoms
                    self.ave_mu[i_struc][i_mom] = ave_val
        return self.ave_mu

    def get_local_averaged_moments(self):
        """
        User function to get and evaluate the locally averaged moments
        :return:
        """
        try:
            self.local_ave_mu
        except ValueError:
            self.local_ave_mu = []
        if not (self.local_ave_mu == []):
            print('self.local_ave_mu is not [] = True')
            return self.local_ave_mu
        else:
            # Calculate averaged moments
            n_struc = len(self.mu)
            self.local_ave_mu = [None]*n_struc
            for i_struc in range(n_struc):
                n_atoms = len(self.mu[i_struc])
                n_mom = len(self.mu[i_struc][0])
                self.local_ave_mu[i_struc] = [None]*n_atoms
                for i_atom in range(n_atoms):
                    self.local_ave_mu[i_struc][i_atom] = [None]*n_mom
                    for i_mom in range(n_mom):
                        mu_ave = self.mu[i_struc][i_atom][i_mom]
                        n_neighbor = \
                            len(self.neighbors[i_struc][i_atom][i_atom])
                        for ii_neighbor, i_neighbor in \
                            enumerate(
                                self.neighbors[i_struc][i_atom][i_atom]):
                            mu_ave += self.mu[i_struc][i_neighbor-1][i_mom]
                        mu_ave /= (n_neighbor+1)
                        self.local_ave_mu[i_struc][i_atom][i_mom] = mu_ave
        return self.local_ave_mu

    def get_mu(self, struc_i='all'):
        """
        User function to get the moments
        :param struc_i: Number of structure, if 'all' get all values
        :return:
        """
        if type(struc_i) == int:
            return self.mu[struc_i]
        else:
            return self.mu

    def get_bonds(self, struc_i='all'):
        """
        User function to get the bonds
        :param struc_i: Number of structure, if 'all' get all values
        :return:
        """
        if type(struc_i) == int:
            return self.bonds[struc_i]
        else:
            return self.bonds

    def get_dists(self, struc_i='all'):
        """
        User function to get the interatomic distances
        :param struc_i: Number of structure, if 'all' get all values
        :return:
        """
        if type(struc_i) == int:
            return self.dists[struc_i]
        else:
            return self.dists

    def get_neighbors(self, struc_i='all'):
        """
        User function to get a list of the neighbors
        :param struc_i: Number of structure, if 'all' get all values
        :return:
        """
        if type(struc_i) == int:
            return self.neighbors[struc_i]
        else:
            return self.neighbors

    def get_min_dist(self, struc_i='all'):
        """
        User function to get the minimal distance
        :param struc_i: Number of structure, if 'all' get all values
        :return:
        """
        if type(struc_i) == int:
            return self.min_dist[struc_i]
        else:
            return self.min_dist

    def get_pf(self, struc_i='all'):
        """
        User function to get the packing fraction
        :param struc_i: Number of structure, if 'all' get all values
        :return:
        """
        if type(struc_i) == int:
            return self.pf[struc_i]
        else:
            return self.pf

    def get_dim(self, struc_i='all'):
        """
        User function to get effective dimensionality of the structure
        which each atom sees
        :param struc_i: Number of structure, if 'all' get all values
        :return:
        """
        if type(struc_i) == int:
            return self.dim[struc_i]
        else:
            return self.dim

    def get_int_coord(self, struc_i='all'):
        """
        User function to get the integer coordination of each atom
        :param struc_i: Number of structure, if 'all' get all values
        :return:
        """
        if type(struc_i) == int:
            return self.int_coord[struc_i]
        else:
            return self.int_coord

    def get_frac_coord(self, struc_i='all'):
        """
        User function to get the fractional coordination of each atom
        :param struc_i:
        :return:
        """
        if type(struc_i) == int:
            return self.frac_coord[struc_i]
        else:
            return self.frac_coord


# bopfox_path = '/home/users/jenkejb7/bopfox_revs/bopfox170/bopfox/src' \
#               '/bopfox'
bopfox_path = 'bopfox'

def _norm_sec_mom_canonicaltb(atoms, orbital, rcut, dcut, a_start=1.,
                              mu_wished=1.,
                              eps=1e-5, i_atom='all', n_mom=4,
                              mem_limit=None, geo_eval=True):
    """
    Function which normalizes the second moment
    :param atoms:
    :param rcut:
    :param dcut:
    :param a_start:
    :param mu_wished:
    :param eps:
    :param i_atom:
    :param n_mom:
    :param mem_limit:
    :param geo_eval:
    :param coordinates_only:
    :return:
    """
    #print("In: _norm_sec_mom_canonicaltb")
    bopatoms = _write_atomsbx(orbital)
    bopbonds = _write_bondsbx(orbital, rcut=rcut, dcut=dcut, r2cut=rcut,
                              d2cut=dcut)

    model = modelsbx(model='test', atomsbx=[bopatoms],
                     bondsbx=[bopbonds],
                     infox_parameters={'moments': n_mom,
                                       'bandwidth': 'azero',
                                       'nexpmoments': 0})

    model.write()
    
    calc = bopcalc.BOPfox(version='bop',
                          printmu_averaged=True, printmu=True,
                          printanbn=True,
                          mem_limit=mem_limit,
                          task='energy', scfsteps=0, efermisteps=0,
                          savelog=False,
                          ignore_errors=True,
                          model='test', modelsbx=model,
                          bopfox=bopfox_path)

    #print("calc:", calc)
    a_start_ini = a_start

    scaled_pos = atoms.get_scaled_positions()

    cell_ini = atoms.get_cell()*a_start_ini
    atoms.set_cell(cell_ini)
    atoms.set_scaled_positions(scaled_pos)
    atoms.set_chemical_symbols('W' + str(atoms.get_number_of_atoms()))

    calc.set_atoms(atoms)

    calc.write_infox_new()

    #print("Next: cal.calculate()")
    calc.calculate()
    #print("Next: mu_start = calc.get_moments(..)")
    mu_start = calc.get_moments(atom_index=i_atom+1, moment=2)
    #print("mu_start:", mu_start)
    while (mu_start is None) or mu_start == 0.:
        if mu_start is None:
            cell_ini *= 2.
        if mu_start == 0.:
            cell_ini *= 0.5
        atoms.set_cell(cell_ini)
        atoms.set_scaled_positions(scaled_pos)
        calc.set_atoms(atoms)
        calc.calculate()
        mu_start = calc.get_moments(atom_index=i_atom+1, moment=2)

    mu_curr = mu_start
    cell_curr = cell_ini[:]

    loop_continue = True
    while loop_continue:
        if mu_curr is not None:
            if mu_curr > 0:
                cell_curr *= mu_curr**(1./10.)
            else:
                cell_curr /= 1.01
        else:
            cell_curr *= 2.
        atoms.set_cell(cell_curr)
        atoms.set_scaled_positions(scaled_pos)
        calc.set_atoms(atoms)
        calc.calculate()
        mu_curr = calc.get_moments(atom_index=i_atom+1, moment=2)
        loop_continue = True
        if mu_curr is None:
            cell_curr *= 2.
        else:
            loop_continue = (abs(mu_curr-mu_wished) > eps)

    an, bn = calc.get_anbn(atom_index=i_atom, moment='all')


    # If a geometrical evaluation is performed later, not all moments are
    # necessary for the output
    if geo_eval:
        return cell_curr, scaled_pos, mu_curr, an, bn
    else:
        #print("Next: calc.get_moments(..)")
        mu_all = calc.get_moments(atom_index=i_atom+1, moment='all')
        return cell_curr, scaled_pos, mu_curr, mu_all, an, bn


def _evaluate_atoms(atom_struc, orbital, rcut, dcut, n_mom=4,
                    mem_limit=None, geo_eval=True, atoms_to_eval='all',
                    a_start=1., store_norm_cells=False):
    """
    :param atoms: List of ASE atom object (means a single structure)
    :param n_mom: Number of moments
    :param mem_limit: Provide a memory limit to prevent unwanted memory
    overflow
    :param geo_eval: Perform a geometrical evaluation?
    :param atoms_to_eval: which atoms should be evaluated?
          'all': evaluate all atoms of the structure
          integer: evaluate only atom with index i (starting from zero)
          list, tuple, numpy array: specify a set of indices
    :return:
    """

    n_atoms_total = atom_struc.get_number_of_atoms()
    #print("n_atoms_total:", n_atoms_total)
    if atoms_to_eval == 'all':
        n_atoms = n_atoms_total
        indices = list(range(n_atoms))
    elif type(atoms_to_eval) == int or type(atoms_to_eval) == float:
        n_atoms = 1
        indices = [int(atoms_to_eval)]
    elif type(atoms_to_eval) == list or type(atoms_to_eval) == tuple or \
            type(atoms_to_eval) == np.ndarray:
        n_atoms = len(atoms_to_eval)
        indices = [None]*n_atoms
        for i in range(n_atoms):
            indices[i] = int(atoms_to_eval[i])
    else:
        print('Unknown type for atoms_to_eval!')
        return
    temp_atoms = atom_struc.copy()

    new_symbols = ['W']*n_atoms_total
    temp_atoms.set_chemical_symbols(new_symbols)

    # Allocate arrays
    mu = [None]*n_atoms
    an = [None]*n_atoms
    bn = [None]*n_atoms
    bonds = [None]*n_atoms
    dists = [None]*n_atoms
    neighbors = [None]*n_atoms
    min_dist = [None]*n_atoms
    pf = [None]*n_atoms
    dim = [None]*n_atoms
    int_coord = [None]*n_atoms
    frac_coord = [None]*n_atoms
    norm_cells = [None]*n_atoms

    for i_i_atom, i_atom in enumerate(indices):
        # Normalize the second moment of each atom
        #print("i_i_atom:", i_i_atom)
        #print("geo_eval:", geo_eval)
        if geo_eval:
            # if a geometrical evaluation is performed later,
            # the complete output is not required
            cell_curr, scaled_pos, mu_curr, an[i_i_atom], bn[i_i_atom] = \
                _norm_sec_mom_canonicaltb(temp_atoms, orbital, rcut, dcut,
                                          a_start=a_start, mu_wished=1.,
                                          eps=1e-5, i_atom=i_atom,
                                          n_mom=n_mom,
                                          mem_limit=mem_limit,
                                          geo_eval=geo_eval)
           # print("cell_curr:", cell_curr)
            # Define the calculator such that the bonds can be extracted
            bopatoms = _write_atomsbx(orbital)
            bopbonds = _write_bondsbx(orbital, rcut=rcut, dcut=dcut,
                                      r2cut=rcut, d2cut=dcut)
            model = modelsbx(model='test', atomsbx=[bopatoms],
                             bondsbx=[bopbonds],
                             infox_parameters={'moments': n_mom,
                                               'bandwidth': 'azero',
                                               'nexpmoments': 0})

            model.write()

            calc = bopcalc.BOPfox(version='bop',
                                  savelog=False,
                                  printmu_averaged=True, printmu=True,
                                  mem_limit=mem_limit, printbonds=True,
                                  task='energy',
                                  scfsteps=0, efermisteps=0,
                                  ignore_errors=True,
                                  model='test', modelsbx=model,
                                  printanbn=True,
                                  bopfox=bopfox_path)
            #print("calc:", calc)
            temp_atoms.set_cell(cell_curr)
            temp_atoms.set_scaled_positions(scaled_pos)
            calc.set_atoms(temp_atoms)
            calc.calculate()
            #print("Done calc.calculate()")
            # bonds[i_i_atom], dists[i_i_atom], neighbors[i_i_atom] are
            # list for each atoms. First index [i_atom] means that the
            # structure was normalized such that the second moment of
            # atom [i_atom] is equal to 1. However bonds are evaluated
            # for all atoms again
            # TODO: Are the information about the other bonds important or
            # should bonds[i_atom] change to bonds[i_atom][i_atom] and
            # so on?
            bonds[i_i_atom], dists[i_i_atom], neighbors[i_i_atom] = \
                calc.get_bonds()
            mu[i_i_atom] = calc.get_moments(atom_index=i_atom+1,
                                            moment='all')
            an[i_i_atom], bn[i_i_atom] = calc.get_anbn(
                atom_index=i_atom, moment='all')
        else:
            #print("n_mom:", n_mom)
            cell_curr, scaled_pos, mu_curr, mu[i_i_atom], an[i_i_atom],\
            bn[i_i_atom] = \
                _norm_sec_mom_canonicaltb(temp_atoms, orbital, rcut, dcut,
                                          a_start=1., mu_wished=1.,
                                          eps=1e-5, i_atom=i_atom,
                                          n_mom=n_mom, mem_limit=mem_limit,
                                          geo_eval=geo_eval)
            #print("Done _norm_sec_mom_canonicaltb")
        # Find minimal distance
        if geo_eval:
            min_dist_at = []
            for j_atom in range(len(dists[i_i_atom])):
                if len(dists[i_i_atom][j_atom]) > 0:
                    min_dist_at.append(np.min(dists[i_i_atom][j_atom]))
            min_dist[i_i_atom] = np.min(min_dist_at)
            # Calculate packing fraction  (should be the same for all,
            # calculate it nevertheless)
            v_cell = temp_atoms.get_volume()
            v_atom = n_atoms * (4./3. * np.pi * (min_dist[i_i_atom]/2.)**3)
            pf[i_i_atom] = v_atom/v_cell
            # Check dimensionality
            dim[i_i_atom] = _check_dimensionality(bonds[i_i_atom])
            # Count number of bonds / integer coordination
            int_coord[i_i_atom] = \
                _integer_coordination(bonds[i_i_atom], dists[i_i_atom],
                                      rcut=rcut)
            # Calculate coordination according to Ralf
            frac_coord[i_i_atom] = \
                _fractional_coordination(dists[i_i_atom], rcut, dcut, n=1)

        if store_norm_cells:
            norm_cells[i_i_atom] = cell_curr

    return mu, an, bn, bonds, dists, neighbors, min_dist, pf, dim, \
           int_coord, frac_coord, norm_cells


def _check_dimensionality(bonds):
    """
    Check dimensionalty of all atoms in a given structure
    :param bonds: list of all bonds for a given structure
    :return:
    """

    dim = 2

    n_atoms = len(bonds)
    for i_atom in range(n_atoms):
        combis = list(itertools.permutations(list(range(len(bonds[i_atom]))), 3))
        for i_combi in range(len(combis)):
            d0 = bonds[i_atom][combis[i_combi][0]]
            d1 = bonds[i_atom][combis[i_combi][1]]
            d2 = bonds[i_atom][combis[i_combi][2]]
            v = np.dot(np.cross(d0, d1), d2)
            if v > 1e-7:
                dim = 3
    return dim


def _integer_coordination(bonds, dists, rcut):
    """
    Calculate the integer coordination number of all atoms in a given
    structure
    :param bonds: list of all bonds for a given structure
    :param dists: list of all distances for a given structure
    :param rcut: Cutoff radius above which distances do not contribute to
    the coordination anymore
    :return:
    """

    n_atoms = len(bonds)

    int_coord = np.zeros(n_atoms)

    for i_atom in range(n_atoms):
        int_coord[i_atom] = 0
        for i_n in range(len(bonds[i_atom])):
            if dists[i_atom][i_n] <= rcut:
                int_coord[i_atom] += 1

    return np.array(int_coord)


def _f_cut(dist, rcut, dcut):
    """
    Cos^2 cutoff function as implemented in bopfox
    :param dist: either one distance or a set of distances
    :param rcut: Choose cutoff value
    :param dcut: Choose dcut value
    :return:
    """

    if type(dist) == float or type(dist) == int\
        or type(dist) == np.float64:

        if dist < (rcut - dcut):
            return 1.
        elif dist < rcut:
            return 0.5 * (np.cos(np.pi *
                                 ((dist - (rcut - dcut))/dcut)) + 1)
        elif dist >= rcut:
            return 0.

    else:
        res = np.zeros(len(dist))
        for d_i, d in enumerate(dist):
            if d < (rcut - dcut):
                res[d_i] = 1.
            elif d < rcut:
                res[d_i] = 0.5 * \
                           (np.cos(np.pi * ((d - (rcut - dcut))/dcut)) + 1)
            elif d >= rcut:
                res[d_i] = 0.
        return res


def _fractional_coordination(dists, rcut, dcut, n=5):
    """
    Calculate the fractional coordination
    :param dists: list of all distances for a given structure
    :param rcut: Choose cutoff value for the cutoff function
    :param dcut: dcut: Choose dcut value for the cutoff function
    :param n: (d/r_nn)**n, Choose n
    :return:
    """

    n_atoms = len(dists)
    frac_coord = np.zeros(n_atoms)

    for i_atom in range(n_atoms):
        frac_coord[i_atom] = 0
        if len(dists[i_atom]) > 0:
            r_nn = min(dists[i_atom])
            for i_n in range(len(dists[i_atom])):
                frac_coord[i_atom] += \
                    _f_cut(dists[i_atom][i_n], rcut, dcut)**n * \
                    (dists[i_atom][i_n]/r_nn)**n

    return np.array(frac_coord)

def _write_atomsbx(orb, valenceelectrons=None):
    """
    Write the atoms.bx File
    :param orb: Which model? 's', 'p', 'd' or 'sp'?
    :return:
    """

    if orb == 's':
        n_orb = 1
        n_e = 1.
        onsitelevels = [0]
    elif orb == 'p':
        n_orb = 3
        n_e = 1.
        onsitelevels = [0]
    elif orb == 'd':
        n_orb = 5
        n_e = 4.8
        onsitelevels = [0]
    elif orb == 'sp':
        n_orb = 4
        n_e = 3.
        onsitelevels = [0, 0]
    elif orb == 'sd':
        n_orb = 6
        n_e = 5.
        onsitelevels = [0, 0]
    else:
        print('Orbital unknown')
        raise

    if valenceelectrons != None:
        n_e = valenceelectrons

    bopatoms = atomsbx(version='v0', atom='W', mass='183.84',
                       valenceorbitals=str(n_orb),
                       valenceelectrons=str(n_e),
                       onsitelevels=onsitelevels)

    return bopatoms

# Original bondsbx
def _write_bondsbx(orb, rcut=3.27390638523000010451,
                   dcut=0.34999999999999997780,
                   d2cut=0.00000000000000000000,
                   r2cut=3.27390638523000010451):
    """
    Write the bonds.bx File
    :param orb: Which model? 's', 'p' or 'd'?
    :return:
    """

    bond = ['W', 'W']
    scaling = [1.]
    pairrepulsion = [0.000, 1.000, 1.000, 1.000]
    eamattraction = [0.000, 1.000, 1.000, 1.000]
    eamcoupling = [1.000, 1.000]
    valence = [orb, orb]

    if orb == 's':
        functions = {'sssigma':'canonicaltb', 'sssigmaoverlap':'canonicaltb'}
        sssigma = [-0.9092227449860677,  -5., 2.081]
        sssigmaoverlap = [0.000, 0.000, 0.000]

        bopbonds = bondsbx(functions=functions, bond=bond, valence=valence
                          , scaling=scaling, sssigma=sssigma
                          , sssigmaoverlap=sssigmaoverlap
                          , pairrepulsion=pairrepulsion
                          , eamattraction=eamattraction
                          , eamcoupling=eamcoupling,
                          rcut=rcut, dcut=dcut, r2cut=r2cut,
                          d2cut=d2cut)
    if (orb=='p'):
        functions = {'ppsigma':'canonicaltb', 'pppi':'canonicaltb',
                     'ppsigmaoverlap':'canonicaltb',
                     'pppioverlap':'canonicaltb'}


        # p_pps = 2
        # p_ppp = -0.5
        p_pps = 2.31
        p_ppp = -0.76

        n_orb = 3.
        #
        k = n_orb / (p_pps ** 2 +  2 * p_ppp ** 2)

        k = np.sqrt(k) * 0.9092227449860677

        p_pps = p_pps*k
        p_ppp = p_ppp*k

        ppsigma = [p_pps, -5., 2.081]
        pppi = [p_ppp,  -5., 2.081]

        # ppsigma = [1.285835137177391, 2.081, 0.000, 0.000]
        # pppi = [-0.6429175685886955,  2.081, 0.000, 0.000]
        ppsigmaoverlap = [0.000, 0.000, 0.000]
        pppioverlap = [0.000, 0.000, 0.000]


        bopbonds = bondsbx(functions=functions, bond=bond, valence=valence
                          , scaling=scaling, ppsigma=ppsigma, pppi=pppi
                          , ppsigmaoverlap=ppsigmaoverlap
                          , pppioverlap=pppioverlap
                          , pairrepulsion=pairrepulsion
                          , eamattraction=eamattraction
                          , eamcoupling=eamcoupling,
                          rcut=rcut, dcut=dcut, r2cut=r2cut,
                          d2cut=d2cut)

    if orb == 'd':

        # p_dds = 0
        # p_ddp = 0
        # p_ddd = 1

        # p_dds = 1.2785
        # p_ddp = -0.5
        # p_ddd = 0.5
        #
        # p_dds = -6
        # p_ddp = 4
        # p_ddd = -1

        # n_orb = 5.
        #
        # k = n_orb / (p_dds ** 2 +  2 * p_ddp ** 2 +  2 * p_ddd ** 2)
        #
        # k = np.sqrt(k) * 0.9092227449860677
        #
        # p_dds = p_dds*k
        # p_ddp = p_ddp*k
        # p_ddd = p_ddd*k

        # ddsigma = [p_dds,  2.081, 0.000, 0.000]
        # ddpi = [p_ddp,  2.081, 0.000, 0.000]
        # dddelta = [p_ddd,  2.081, 0.000, 0.000]

        functions = {'ddsigma':'powerlaw', 'ddpi':'powerlaw',
                     'dddelta':'powerlaw',
                     'ddsigmaoverlap':'powerlaw',
                     'ddpioverlap':'powerlaw',
                     'dddeltaoverlap':'powerlaw'}
        ddsigma = [-1.458,  -5., 2.081]
        ddpi = [0.972,  -5., 2.081]
        dddelta = [-0.243,  -5., 2.081]
        ddsigmaoverlap = [0.000, 0.000, 0.000]
        ddpioverlap = [0.000, 0.000, 0.000]
        dddeltaoverlap = [0.000, 0.000, 0.000]

        bopbonds = bondsbx(functions=functions, bond=bond, valence=valence
                          , scaling=scaling, ddsigma=ddsigma, ddpi=ddpi
                          , dddelta=dddelta, ddsigmaoverlap=ddsigmaoverlap
                          , ddpioverlap=ddpioverlap
                          , dddeltaoverlap=dddeltaoverlap
                          , pairrepulsion=pairrepulsion
                          , eamattraction=eamattraction
                          , eamcoupling=eamcoupling,
                          rcut=rcut, dcut=dcut, r2cut=r2cut,
                          d2cut=d2cut)

    if orb == 'sp':
        functions = {'sssigma':'powerlaw', 'spsigma':'powerlaw',
                     'pssigma':'powerlaw', 'ppsigma':'powerlaw',
                     'pppi':'powerlaw',
                     'sssigmaoverlap':'powerlaw',
                     'spsigmaoverlap':'powerlaw',
                     'pssigmaoverlap':'powerlaw',
                     'ppsigmaoverlap':'powerlaw',
                     'pppioverlap':'powerlaw'}
        # sssigma = [-0.5899816232642112, 2.081, 0.000, 0.000]
        # spsigma = [0.8343600131711415,  2.081, 0.000, 0.000]
        # pssigma = [-0.8343600131711415,  2.081, 0.000, 0.000]
        # ppsigma = [1.1799632465284224,  2.081, 0.000, 0.000]
        # pppi = [-0.2949908116321056,  2.081, 0.000, 0.000]
        sssigmaoverlap = [0.000, 0.000, 0.000]
        spsigmaoverlap = [0.000, 0.000, 0.000]
        pssigmaoverlap = [0.000, 0.000, 0.000]
        ppsigmaoverlap = [0.000, 0.000, 0.000]
        pppioverlap = [0.000, 0.000, 0.000]

        p_sss = -1
        p_sps = 1.31
        p_pss = -1.31
        p_pps = 2.31
        p_ppp = -0.76

        n_orb = 4.

        k = n_orb / (p_sss ** 2 + 2 * p_sps ** 2 + p_pps ** 2 + 2 *
                     p_ppp ** 2)

        k = np.sqrt(k) * 0.9092227449860677

        sssigma = [k*p_sss, -5., 2.081]
        spsigma = [k*p_sps,  -5., 2.081]
        pssigma = [k*p_pss,  -5., 2.081]
        ppsigma = [k*p_pps,  -5., 2.081]
        pppi = [k*p_ppp,  -5., 2.081]

        bopbonds = bondsbx(functions=functions, bond=bond, valence=valence,
                           scaling=scaling, sssigma=sssigma,
                           spsigma=spsigma, pssigma=pssigma,
                           ppsigma=ppsigma, pppi=pppi,
                           sssigmaoverlap=sssigmaoverlap,
                           spsigmaoverlap=spsigmaoverlap,
                           pssigmaoverlap=pssigmaoverlap,
                           ppsigmaoverlap=ppsigmaoverlap,
                           pppioverlap=pppioverlap,
                           pairrepulsion=pairrepulsion,
                           eamattraction=eamattraction,
                           eamcoupling=eamcoupling,
                           rcut=rcut, dcut=dcut, r2cut=r2cut,
                           d2cut=d2cut)

    if orb == 'sd':
        functions = {'sssigma':'powerlaw', 'sdsigma':'powerlaw',
                     'dssigma':'powerlaw', 'ddsigma':'powerlaw',
                     'ddpi':'powerlaw', 'dddelta':'powerlaw',
                     'sssigmaoverlap':'powerlaw',
                     'sdsigmaoverlap':'powerlaw',
                     'dssigmaoverlap':'powerlaw',
                     'ddsigmaoverlap':'powerlaw',
                     'ddpioverlap':'powerlaw',
                     'dddeltaoverlap': 'powerlaw'}

        p_sss = -2#-8
        p_sds = -1#-2
        p_dds = -10#-6
        p_ddp = 2#4
        p_ddd = -1

        n_orb = 6.

        k = n_orb/(p_sss**2 + 2*p_sds**2 + p_dds**2 + 2*p_ddp**2 +
                   2*p_ddd**2)
        k = np.sqrt(k) * 0.9092227449860677

        sssigma = [p_sss*k, -5., 2.081]
        sdsigma = [p_sds*k,  -5., 2.081]
        dssigma = [p_sds*k,  -5., 2.081]
        ddsigma = [p_dds*k,  -5., 2.081]
        ddpi = [p_ddp*k,  -5., 2.081]
        dddelta = [p_ddd*k,  -5., 2.081]
        sssigmaoverlap = [0.000, 0.000, 0.000]
        sdsigmaoverlap = [0.000, 0.000, 0.000]
        dssigmaoverlap = [0.000, 0.000, 0.000]
        ddsigmaoverlap = [0.000, 0.000, 0.000]
        ddpioverlap = [0.000, 0.000, 0.000]
        dddeltaoverlap = [0.000, 0.000, 0.000]

        bopbonds = bondsbx(functions=functions, bond=bond, valence=valence,
                           scaling=scaling, sssigma=sssigma,
                           sdsigma=sdsigma, dssigma=dssigma,
                           ddsigma=ddsigma, ddpi=ddpi, dddelta=dddelta,
                           sssigmaoverlap=sssigmaoverlap,
                           sdsigmaoverlap=sdsigmaoverlap,
                           dssigmaoverlap=dssigmaoverlap,
                           ddsigmaoverlap=ddsigmaoverlap,
                           ddpioverlap=ddpioverlap,
                           dddeltaoverlap=dddeltaoverlap,
                           pairrepulsion=pairrepulsion,
                           eamattraction=eamattraction,
                           eamcoupling=eamcoupling,
                           rcut=rcut, dcut=dcut, r2cut=r2cut,
                           d2cut=d2cut)

    # bopio.write_bondsbx([bopbonds], filename='bonds.bx', update=False)

    return bopbonds


def normalize_coordinates(coord_unnormed, global_coordinates,
                          min_val=None, max_val=None):

    if global_coordinates:
        min_val = np.min(coord_unnormed)
        max_val = np.max(coord_unnormed)
        coord_normed = (np.array(coord_unnormed) - min_val) / \
                       (max_val - min_val)

    else:
        if min_val == None:
            min_val = 1e99
            for i in range(len(coord_unnormed)):
                for j in range(len(coord_unnormed[i])):
                    if coord_unnormed[i][j][j] < min_val:
                        min_val = coord_unnormed[i][j][j]

        if max_val == None:
            max_val = -1e99
            for i in range(len(coord_unnormed)):
                for j in range(len(coord_unnormed[i])):
                    if coord_unnormed[i][j][j] > max_val:
                        max_val = coord_unnormed[i][j][j]

        coord_normed = [None] * len(coord_unnormed)
        for i in range(len(coord_unnormed)):
            coord_normed[i] = (np.array(
                coord_unnormed[i]) - min_val) / (
                                     max_val - min_val)

    return coord_normed


